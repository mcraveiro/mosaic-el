(load "inf-snd.el")

(add-hook 'snd-scheme-mode-hook
	  '(lambda ()
	    (define-key (current-local-map) "\C-co" 'snd-send-buffer)
	    (define-key (current-local-map) "\C-cr" 'snd-send-region)
	    (define-key (current-local-map) "\C-ce" 'snd-send-definition)))

(add-to-list 'auto-mode-alist '("\\.scm" . snd-scheme-mode))

;; snd 18.1
;; (setq inf-snd-scheme-program-name "/usr/bin/snd")

;; snd 19.6
;(setq inf-snd-scheme-program-name "/usr/local/bin/snd -b")
(setq inf-snd-scheme-program-name "/usr/local/bin/snd") 

(global-set-key (kbd "s-K") (lambda () (interactive) (save-window-excursion (kill-snd))))

(global-set-key (kbd "<f8>")
		(lambda ()
		  (interactive)
		  (if (eq major-mode 'snd-scheme-mode)
		      (snd-send-buffer)
		    (eval-buffer))))

(global-set-key (kbd "C-<f8>")
		(lambda ()
		  (interactive)
		  (backtrace)
		  (switch-to-buffer "*Messages*")))

(global-set-key (kbd "<f9>") (lambda () (interactive) (kill-snd)))
(global-set-key (kbd "<f10>") (lambda ()
				(interactive)
				(call-interactively #'run-snd-scheme)
				;; hide snd window
				(run-at-time 1.0 nil #'raise-frame)))


