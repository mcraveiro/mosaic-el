#+TITLE: Notes on applying the Sequitur algorithm to music

My project [[file:scheme-mosaic.org][SchemeMosaic]] includes an implementation of the
Nevill-Manning "Sequitur" procedure. See http://www.sequitur.info/ for
more information on this amazing algorithm.

Sequitur analyzes its input (a sequence of discrete symbols) and
recovers a deterministic context-free grammar whose rules re-generate
the original sequence. This grammar's production rules capture all the
nested repetitions and branching found during scanning. Automated
recovery of branching structure requires optional "oblivious
reparsing" (see the original [[http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.52.9964&rep=rep1&type=pdf][paper on Sequitur]]). This option is not
yet implemented.

Sequitur is not guaranteed to produce an optimal grammar. My
implementation is not linear-time but should suffice for sequences
of musically usable length.

I've done some experiments applying Sequitur to analysis and
resynthesis of sequences of sound slices, using these slices property
association lists as the discrete symbols of the sequence to be
analyzed, and then matching slices together based on similarity of
properties.

Let's listen. Here are the opening bars to Zoltan Kodaly's Sonata for
Solo Cello:

 - [[http://xelf.me/kodaly.mp3][Kodaly snippet]]

Here's what the graph of detected MIDI note numbers looks like:

 - [[http://xelf.me/kodaly-2.png][Image of graph]]

You can resynthesize the whole structure using either the samples you
started with, or swap in different samples (see below). You can also
generate variations on the structure. 

 - [[http://xelf.me/kodaly-resynthesized-1.mp3][Kodaly resynthesized from structure]]. Because each MIDI note in the
   structure can match more than one source audio snippet, you get
   artifacts caused by only the first matching occurrence being
   repeated. However, the point is to alter the structure, not remake
   the original file. That brings us to our next example.

 - [[http://xelf.me/kodaly-resynthesized-2.mp3][Resynthesis of Kodaly with variation on structure]]. I chose letters
   from Sequitur's output grammar, which uses variable names from the
   alphabet to identify points of structure. 

#+BEGIN_SRC
(define *kodaly-resynth-pattern*
  '(A I L F B
      A I L F B
      J J G G G G G
      A I L F B
      A I L F B
      J J G G G G G
      E E E
      A I L F B
      E E E
      A I L F B))

(define* (resynthesis-test-1a)
  (let* ((regions (split-sound-to-regions
		   *kodaly-slice-size*
		   (open-sound *kodaly-file*)
		   0)))
    (map region-note* regions)
    (let ((synth (make-synth regions)))
      (with-synth synth
		  (merge-sound (new-sound :channels 2) 0
			       (generate-regions synth
						 *kodaly-resynth-pattern*))))))
#+END_SRC

The code above may make more sense if you look at the output of
Sequitur on the Kodaly snippet:

#+BEGIN_SRC
 (A (((note . 60)) ((note . 58))))
 (B (((note . 62)) ((note . 62))))
 (C (((note . 51)) ((note . 49))))
 (D (((note . 59)) ((note . 60))))
 (E (((note . 59)) ((note . 59))))
 (F (((note . 72)) ((note . 73))))
 (G (((note . 69)) ((note . 69))))
 (H (((note . 58)) ((note . 60))))
 (I (((note . 60)) D))
 (J (((note . 66)) ((note . 65))))
 (K (((note . 59)) ((note . 61))))
 (L (((note . 73)) ((note . 61))))
 (M (((note . 83)) ((note . 83))))))
 (AXIOM^ (((note 66)) ((note 40)) ((note 58)) ((note 66)) ((note
58)) ((note 52)) ((note 48)) C ((note 60)) ((note 50)) ((note
47)) ((note 56)) ((note 53)) ((note 53)) ((note 49)) ((note
59)) ((note 58)) A ((note 57)) A I L F B ((note 61)) B ((note 68)) G
C ((note 50)) ((note 53)) ((note 52)) D E ((note 48)) E H ((note
61)) ((note 73)) F ((note 61)) ((note 61)) ((note 61)) ((note
60)) ((note 62)) ((note 81)) ((note 61)) ((note 59)) G G ((note
49)) ((note 52)) ((note 42)) ((note 55)) H ((note 34)) H ((note 54)) I
F ((note 82)) ((note 63)) ((>note 64)) ((note 66)) J ((note
64)) ((note 78)) ((note 58)) K ((note 77)) ((note 65)) ((note
59)) ((note 54)) ((note 54)) ((note 52)) J K L ((note 73)) ((note
77)) ((note 59)) ((note 65)) M M ((note 83)) ((note 71))))
#+END_SRC

It consists of a series of production rules. The variable (A, B, C...)
on the left expands into the data that follows it. If the result
contains more variables, replace them too, until all that's left are
notes. The special rule called AXIOM^ expands into the entire original
sequence. (It is the default pattern used to reconstruct the first
"complete" example above.)

So, I looked at this output to choose what variables to give in
*KODALY-RESYNTH-PATTERN*. Notice now I picked out "A I L F B" from the
axiom on purpose.

For even more fun, you can swap the audio of two synths after
structural analysis, in order to map one sound's timbres onto the
other's pitch structure. See code below.

 - [[http://xelf.me/whitney.mp3][Whitney Houston snippet]]
 - [[http://xelf.me/whitney-as-kodaly.mp3][Whitney mapped onto Kodaly]]
 - [[http://xelf.me/kodaly-as-whitney.mp3][Kodaly mapped onto Whitney]]
 - [[http://xelf.me/whitney-as-kodaly-improved.mp3][Whitney mapped onto Kodaly]], with pitch-shifting to correct
   suboptimal matches. The result isn't perfect, but the melody can be
   discerned. I suspect it will work better on rhythms, and when the
   pitches of the source and target corpora overlap more. Also,
   matching on a richer set of discretized properties (such as whether
   a slice fits the profile of a kick drum, cymbal, or snare) should
   produce better results. I'll update this page with news.
   
#+BEGIN_SRC
(define* (resynthesis-test-2)
  (let* ((kodaly-regions (split-sound-to-regions
		  *kodaly-slice-size*
		  (open-sound *kodaly-file*)
		  0))
	 (whitney-regions (split-sound-to-regions
		   *kodaly-slice-size*
		   (open-sound "/home/dto/Desktop/whitney-autotalent-snippet.wav")
		   0)))
    (map region-note* kodaly-regions)
    (map region-note* whitney-regions)
    (let* ((kodaly (make-synth kodaly-regions))
	   (whitney (make-synth whitney-regions)))
      (let* ((kodaly->whitney
	      (with-synth kodaly
			  (set! (kodaly 'regions) whitney-regions)
			  (generate-regions kodaly #f)))
	     (whitney->kodaly
	      (with-synth whitney
			  (set! (whitney 'regions) kodaly-regions)
			  (generate-regions whitney #f))))
	(list (merge-sound (new-sound :channels 2) 0 whitney->kodaly)
	      (merge-sound (new-sound :channels 2) 0 kodaly->whitney))))))
#+END_SRC

The gaps result when a SchemeMosaic synth object cannot match its
audio to the structure being generated. More work is needed in this
area.

The structure analyzer needs improvement too, but it does find notes
and intervals that repeat:

 - [[http://xelf.me/kodaly.png][Image of waveform with structure]]

The letters below the waveform represent places where variables showed
up in Sequitur's output, laid out roughly on the waveform of the
sonata's first ten seconds. An asterisk "*" represents variables or
areas containing note 47 (see below). The period "."  represents a
note value that isn't part of a variable, i.e. no repetition structure
was detected. The rapid notes toward the ending probably explain the
lack of variables in roughly the last third of the axiom. It appears
that Sequitur can find empirical notes by encoding (into variables)
sequences where the same pitch occurs across a series of sound
particles. Rules "I" and "H" seem to identify a descending interval
and an ascending interval that are repeated.

(The letter names in the above image and explanation do not match the
variable names previously given, because an earlier version of the
software was used to make the image.)

My next task is applying Sequitur to rhythmic elements, and driving
resynthesis with an "improvisational" user-scored genetic algorithm.
