;;; elo.el --- simple project scripting for emacs   -*- lexical-binding: t; -*-

;; Copyright (C) 2016 by David O'Toole
;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; License: GPLv3

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License
;; (Version 3) as published by the Free Software Foundation. 

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program, in a text file called LICENSE. If not, see
;; <https://www.gnu.org/licenses/>.

;;; <commentary> 

;; In building and developing a project I need to issue many shell
;; commands. With few exceptions, the commands are individually simple
;; one-liners such as:
 
;;  - setting environment variables
;;  - invoking external programs with simple arguments (options, filenames)
;;  - copying/archiving/extracting files
;;  - applying and reversing simple patches
;;  - checking things out from git
;;  - printing information / errors
;;  - cleaning up after other commands

;; But my development environment is Emacs, not the shell. Hence I am
;; building a simple project scripting system in Emacs Lisp.

;;  - Elo is an imperative scripting utility, not a declarative build
;;    system; but it can be used to invoke declarative build systems (GNU
;;    Make, ASDF, etc)
;;  - Elo is not a shell; it is an Emacs Lisp program which can invoke
;;    simple shell commands, arbitrary elisp commands, and compile simple
;;    shell scripts.
;;  - An Elo project consists of simple linear Plans which are a series
;;    of Actions. These are defined the new elisp object system "EIEIO"
;;    and can be extended by the user in this way.
;;  - Actions can be arbitrary Emacs Lisp, and may take steps toward
;;    building a result, or simply things the developer wants to do in
;;    Emacs while developing:
;;    - Ensure network connections or status of daemons
;;    - Open remote interactive shells and dired
;;    - Interoperate with other Emacs facilities
;;  - A plan consisting only of Shell Actions can be compiled into a
;;    shell script. Delivering this file means the end-user doesn't need
;;    Emacs if simply building the project.
;;  - By default Elo executes actions "safely" meaning that underlying
;;    commands are printed instead of being carried out.
;;  - Even when executing "unsafely", Elo should attempt in all cases to
;;    prevent destruction of data by making backup files and carefully
;;    handling errors.
;;  - Dependency management is simple. Each Action in a plan depends on
;;    all prior Actions being completed. If an Action encounters an
;;    error, the user is alerted and can initiate cleanup, fix the
;;    situation, and restart the Action or Plan if needed.
;;  - Elo is verbose and logs progress and outcomes to a special buffer.

;;; <required libraries> 

(require 'rx)
(require 'cl)
(require 'eieio)

;;; <logging>

(defvar elo-log-indentation 0)

(defmacro* elo-indenting (&body body)
  `(let ((elo-log-indentation (1+ elo-log-indentation)))
     ,@body))

(defun elo-indentation-space ()
  (make-string elo-log-indentation ?\ ))

;;; <safety>

(defvar* safely-p t)

(defun safely-p () safely-p)

(defmacro* safely (&body body)
  `(let ((safely-p t)) ,@body))

(defmacro* unsafely (&body body)
  `(let ((safely-p nil)) ,@body))

;;; <font locking>

(defvar* elo-font-lock-keywords
  `((,(rx (sequence "(" (group "defvar*")
		   (one-or-more space)
		   (group (one-or-more (not (any space))))))
      (1 font-lock-keyword-face)
      (2 font-lock-variable-name-face))
    (,(rx (sequence "(" (group "defun*")
		   (one-or-more space)
		   (group (one-or-more (not (any space))))))
      (1 font-lock-keyword-face)
      (2 font-lock-function-name-face))))

(defun elo-do-font-lock ()
  (interactive)
  (font-lock-add-keywords nil elo-font-lock-keywords))

(defun elo-insinuate-emacs ()
  (interactive)
  (elo-do-font-lock))

;;; <projects and files>

(defvar* elo-directory "~/src/")

(defun elo-file (name)
  (expand-file-name name elo-directory))

;;; <the special elo buffer>

(defvar* elo-buffer-name "*elo*")
(defun elo-buffer () (get-buffer-create elo-buffer-name))

(defmacro with-elo-buffer (&body body)
  `(save-window-excursion 
     (switch-to-buffer (elo-buffer))
     ,@body))

(defun elo-clear-log ()
  (with-current-buffer (elo-buffer)
    (delete-region (point-min) (point-max))))

(defun elo-log (format-string &rest args)
  (with-current-buffer (elo-buffer)
    (goto-char (point-max))
    (insert (elo-indentation-space))
    (insert (apply #'format format-string args))
    (insert "\n")))

;;; <calling external commands>

(defun* elo-call (string &optional (output-buffer t))
  (if (safely-p)
      (elo-log "Safely logging without executing: %s" string)
      (call-process-shell-command string nil (list output-buffer t) t)))

(defun* elo-shell-command (command &optional (buffer (elo-buffer)))
  (elo-log "Shell command: %s" command)
  (switch-to-buffer buffer)
  (elo-call command buffer))

;;; <class elo-action>

(defclass elo-action ()
  ((started-p :initform nil :initarg :started-p :accessor elo-started-p)
   (completed-p :initform nil :initarg :completed-p :accessor elo-completed-p)
   (running-p :initform nil :initarg :running-p :accessor elo-running-p)
   (status :initform nil :initarg :status :accessor elo-status)))

(defmethod object-print ((action elo-action) &optional strings)
  (apply #'call-next-method action
         (cons " <>" strings)))

(defmethod elo-print ((action elo-action) &optional verbose-p)
  (eieio-object-class-name action))
          
(defmethod elo-mark-completed ((action elo-action))
  (with-slots (completed-p) action
    (setf completed-p t)))

(defmethod elo-mark-completed :after ((action elo-action))
  (elo-log "Marked action %s as completed." (elo-print action)))

(defmethod elo-start ((action elo-action))
  (with-slots (completed-p running-p started-p) action
    (assert (not completed-p))
    (setf running-p t
	  started-p t)))

(defmethod elo-stop ((action elo-action))
  (with-slots (running-p) action
    (assert (not (null running-p)))
    (setf running-p nil)))

(defmethod elo-suspended-p ((action elo-action))
  (with-slots (completed-p) action
    (and (not completed-p)
	 (elo-processing-p action))))
	  
(defmethod elo-succeeded-p ((action elo-action))
  (with-slots (running-p completed-p status) action
    (and completed-p
	 (not running-p))))

(defmethod elo-failed-p ((action elo-action))
  (with-slots (running-p completed-p status) action
    (and (not running-p)
	 (not completed-p))))

(defmethod elo-processing-p ((action elo-action)) nil)

(defmethod elo-prepare ((action elo-action)) nil)

(defmethod elo-prepare :before ((action elo-action))
  (elo-log "Preparing action %s ..." (elo-print action)))

(defmethod elo-prepare :after ((action elo-action))
  (elo-log "Preparing action %s ... Done." (elo-print action)))

(defmethod elo-perform ((action elo-action))
  (elo-log "Note: performing empty action.")
  (elo-mark-completed action))

(defmethod elo-perform :before ((action elo-action))
  (elo-log "Performing action %s ..." (elo-print action))
  (elo-start action))

(defmethod elo-perform :after ((action elo-action))
  (elo-stop action)
  (elo-log "Performing action %s ... Done." (elo-print action)))

(defmethod elo-report ((action elo-action))
  (let ((text (cond ((elo-succeeded-p action)
		     "Action %s succeeded with status %s")
		    ((elo-suspended-p action)
		     "Action %s suspended with status %s while process runs.")
		    ((elo-failed-p action)
		     "Action %s failed with status %s"))))
    (elo-log text (elo-print action) (elo-status action))))

(defmethod elo-execute ((action elo-action))
  (with-slots (status completed-p) action
    (elo-prepare action)
    (setf status
	  (elo-perform action))
    (elo-report action)))

(defmethod elo-execute :after ((action elo-action))
  (show-buffer nil (elo-buffer)))

;;; <class elo-shell-action>

(defclass elo-shell-action (elo-action)
  ((command :initform nil :initarg :command :accessor elo-command)
   (command-string :initform nil :initarg :command-string :accessor elo-command-string)
   (output-buffer :initform nil :initarg :output-buffer :accessor elo-output-buffer)))

(defmethod elo-find-command-string ((action elo-shell-action))
  (elo-command action))

(defmethod elo-report :after ((action elo-shell-action))
  (with-slots (output-buffer) action
    (when output-buffer (elo-log "%s" output-buffer))))

(defmethod elo-prepare ((action elo-shell-action))
  (setf (elo-command-string action)
	(elo-find-command-string action)))

(defmethod elo-perform ((action elo-shell-action))
  (with-temp-buffer 
    (with-slots (command-string output-buffer) action
      (elo-shell-command command-string (elo-buffer)))))

(defmethod elo-perform :after ((action elo-shell-action))
  (with-slots (output-buffer status) action
    (cond ((or (and (integerp status)
		    (zerop status))
	       (null status))
	   (elo-mark-completed action))
	  ((or (integerp status) (stringp status))
	   (error "CALL-PROCESS returned error %s" status)))))

;;; <asynchronous shell commands>

(defvar* elo-process nil)

(defun elo-process-buffer () (get-buffer-create "*elo-process*"))

(defun elo-process-p ()
  (not (null elo-process)))

(defun elo-process-live-p ()
  (process-live-p elo-process))

(defun elo-process-exit-p ()
  (eq 'exit (process-status elo-process)))

(defun elo-process-running-p ()
  (eq 'run (process-status elo-process)))

(defun elo-process-signal-p ()
  (eq 'signal (process-status elo-process)))

(defun elo-start-process (command)
  (unless (safely-p)
    (setf elo-process
	  (start-process "elo" (elo-process-buffer) "sh" "-x" "-c" command))))

(defun elo-kill-process ()
  (interactive)
  (kill-process elo-process))

(defclass elo-process-action (elo-shell-action) ())

(defmethod elo-perform ((action elo-process-action))
  (assert (not (elo-process-live-p)))
  (elo-start-process (elo-command-string action))
  (if (elo-process-live-p)
      (elo-log "Started process %s" elo-process)
      (error "Failed to start process.")))

(defmethod elo-processing-p ((action elo-process-action))
  (elo-process-live-p))

;;; <making shell scripts>

(defun make-shell-script (actions)
  (with-temp-buffer
    (dolist (action actions)
      (insert (elo-find-command-string action))
      (insert "\n"))
    (buffer-substring-no-properties (point-min) (point-max))))

;;; <setting environment variables>

(defun elo-environment-variable-name (var)
  (with-temp-buffer
    (insert (symbol-name var))
    (upcase-region (point-min) (point-max))
    (goto-char (point-min))
    (replace-string "-" "_")
    (buffer-substring-no-properties (point-min) (point-max))))

(defclass elo-setenv-action (elo-shell-action)
  ((name :initform nil :initarg :name)
   (value :initform nil :initarg :value)))

(defmethod initialize-instance :after ((action elo-setenv-action) &key)
  (with-slots (name) action
    (assert (not (null name)))
    (when (symbolp name)
      (setf name (elo-environment-variable-name name)))))

(defmethod elo-find-command-string ((action elo-setenv-action))
  (with-slots (name value) action
    (format "export %s=%s" name value)))

(defmethod elo-perform ((action elo-setenv-action))
  (with-slots (name value) action
    (setenv name value t)))

(defun elo-env (&rest args)
  (apply #'make-instance 'elo-setenv-action args))

;;; <changing directories>

(defclass elo-cd-action (elo-shell-action)
  ((directory :initform nil :initarg :directory)))

(defmethod elo-perform ((action elo-cd-action))
  (with-slots (directory) action
    ;; use internal emacs cd 
    (cd directory)))

(defmethod elo-find-command-string ((action elo-cd-action))
  (with-slots (directory) action
    ;; still works when compiled to shell script
    (format "cd %s" directory)))

;;; <patching files>

(defvar* elo-patch-default-options "--backup --verbose --force --context")

(defun* elo-patch-options (&optional reverse-p)
  (concat (if reverse-p "--reverse " "")
	  elo-patch-default-options))

(defun elo-patch-command (file patch &optional reverse-p)
  (format "patch %s %s %s" (elo-patch-options reverse-p) file patch))

(defclass elo-patch-action (elo-shell-action)
  ((reverse-p :initform nil :initarg :reverse-p)
   (target-file :initform nil :initarg :target-file)
   (patch-file :initform nil :initarg :patch-file)))

(defmethod elo-find-command-string ((action elo-patch-action))
  (with-slots (target-file patch-file reverse-p) action
    (elo-patch-command target-file patch-file reverse-p)))

;;; <class elo-plan>

(defclass elo-plan (elo-action)
  ((name :initform "Untitled plan" :initarg :name :accessor elo-plan-name)
   (actions :initform nil :initarg :actions :accessor elo-plan-actions)
   (current-action :initform nil :initarg :current-action :accessor elo-plan-current-action)))

(defmethod elo-processing-p ((plan elo-plan))
  (elo-running-p plan))
    
(defmethod elo-next-action ((plan elo-plan))
  (with-slots (actions current-action) plan
    (let ((n (position current-action actions :test 'eq)))
      (first (rest (nthcdr n actions))))))

(defmethod elo-step-plan ((plan elo-plan))
  (with-slots (current-action actions completed-p running-p) plan
    (cond
      ((elo-completed-p plan)
       (error "Cannot step completed plan."))
      ;;
      ((and (null current-action)
	    (not (elo-completed-p plan)))
       (setf current-action (first actions)))
      ;;
      ((not (elo-started-p current-action))
       (elo-execute current-action))
      ;;
      ((elo-processing-p current-action) 
       (elo-log "Note: current-action %s is still processing." (elo-print current-action)))
      ;;
      ((elo-failed-p current-action)
       (elo-report current-action)
       (error "Plan step %s failed." (elo-print current-action)))
      ;;
      ((elo-succeeded-p current-action)
       (elo-log "Plan step %s succeeded." (elo-print current-action))
       (setf current-action
	     (elo-next-action plan))
       (when (null current-action)
	 (elo-mark-completed plan)))
      ;;
      ((elo-completed-p plan)
       (elo-log "Plan %s completed." plan)))))

(defvar* elo-timer nil)
(defvar* elo-current-plan nil)

(defun elo-try-continue ()
  (when elo-timer
    (unless (elo-processing-p elo-current-plan)
      (elo-perform elo-current-plan))))

(defmethod elo-execute :before ((plan elo-plan))
  (setf elo-current-plan plan)
  (setf elo-timer (run-at-time 1 2 #'elo-try-continue)))

(defmethod elo-mark-completed :after ((plan elo-plan))
  (when elo-timer (cancel-timer elo-timer))
  (setf elo-current-plan nil))
	   
(defmethod elo-perform ((plan elo-plan))
  (with-current-buffer (elo-buffer)
    (elo-clear-log)
    (block suspended
      (loop while (not (elo-completed-p plan))
	 do (progn
	      (elo-step-plan plan)
	      (when (elo-processing-p (elo-plan-current-action plan))
		(return-from suspended nil)))))
    (when (not (elo-completed-p plan))
      (elo-log "Plan suspended while external process runs."))))

;;; <defining plans>

(defvar* elo-action-abbrevs
    '((cd . elo-cd-action)
      (var . elo-setenv-action)
      (shell . elo-shell-action)
      (process . elo-process-action)
      (patch . elo-patch-action)))

(defun elo-action-class-from-abbrev (symbol)
  (cdr (assoc symbol elo-action-abbrevs)))

(defun elo-action-from-abbrev (symbol args)
  (apply #'make-instance
	 (elo-action-class-from-abbrev symbol)
	 args))

(defun make-keyword (string)
  (intern (concat ":" string)))

(defun elo-abbrev-flet-clause (symbol)
  `(,symbol (&rest args)
     (funcall #'elo-action-from-abbrev ',symbol args)))

(defmacro with-elo-abbrevs (abbrevs body)
  `(labels ,(mapcar #'elo-abbrev-flet-clause abbrevs) ,body))

;; <macro defplan>
(defmacro defplan (name superclasses slot-declarations actions*)
  `(progn 
     (defclass ,name ,(or superclasses '(elo-plan)) ,slot-declarations)
     (defmethod initialize-instance :after ((plan ,name) &key)
		(with-slots (actions) plan
		  (with-elo-abbrevs (cd var shell process patch)
		    (setf actions (list ,@actions*)))))))

;;; Copying directories
;;; Invoking GNU Make
;;; Opening remote shells and files

;; (mapcar #'trace-function '(elo-abbrev-flet-clause elo-action-from-abbrev elo-action-class-from-abbrev with-elo-abbrevs defplan))

;; (mapcar #'trace-function '(elo-mark-completed elo-start elo-stop elo-succeeded-p elo-failed-p elo-report elo-perform elo-file elo-log elo-command elo-patch-options elo-patch-command elo-install-patch elo-find-command-string elo-uninstall-patch))

(provide 'elo)
;;; elo.el ends here


