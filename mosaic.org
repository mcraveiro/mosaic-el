* Current Tasks
** TODO follow amplitude envelope of original slice
** TODO allow option to merge loaded database


** TODO don't auto populate looper
** TODO [#B] Loop Wizard
** TODO [#A] Import beatloops/breaks from Waveworld and jam with them
** TODO neuraljam with loops
   
** TODO interactively loopjam with ecasound and try different database matches / programs
** TODO looper output WAV should always be a proper slice
** TODO change commands to be MOSAIC-START-LOOPING and MOSAIC-START-PLAYING, and change buttons to match
** TODO don't loop play in ecasound by default
** TODO don't record to disk from ecasound while looping
** TODO on MOSAIC-RENDER-LOOPER, Export exact loop segment from looper (temporarily disable loops , or specify play end)
** TODO button to copy output resources to stash sequence

** TODO allow setting volume and pan in looper

** TODO build up song loops improvisationally and import into Audacity or Ardour or LMMS for further work

  
** TODO Import community orchestral samples with auto-tagging
** TODO EL: create a cappella category database

** DONE diagnose and fix memory allocation creep during long imports

** TODO [#C] EL: show eca transport in header line, with timestamp

* Other Tasks

** TODO Allow CHOP to have a new sequence each time; (CHOP* PATTERNS)

** TODO [#A] beat quantizer
** TODO implement "swing" tempo

** TODO [#A] XM slice/import/render wizard
** TODO [#A] Soundfont wizard
** TODO [#A] Import found beats into Stomper wizard
** TODO [#C] Add SCREW-MATCH-SYNTH to complete SEARCH/STRETCH ... SCREW/SHIFT option palette, use this for drums
** TODO [#A] use orchestra sequencer framework? simple tracker instrument
** TODO [#A] Processor Wizard interactively chooses Scheme expressions for cells
** DONE [#A] Don't set buffer-modified-p when clicking cell 

** TODO [#A] allow matching on pitch class
** TODO [#B] write some common use case shortcuts for WEIGHTED-DISTANCE and allow choosing from score Wizard
** TODO [#B] Import drums from Waveworld and try in stomper

** TODO [#B] Try adding support for analog filters
** TODO [#B] Scheme wizard
** TODO [#B] Audio setup
*** List audio devices
*** Choose audio device

** TODO [#B] merge indices hierarchically to create different search domains (any subset of the database)
** TODO [#B] each search is a new mergeable index
** TODO [#C] EL: perform search and audition in Ecasound
** TODO [#C] ensure session-local engine/other settings are preserved
** TODO [#C] Use WHISPER processing to remove/isolate harmonic part of sound for analysis/resynthesis
*** voiced->unvoiced amp fftsize r tempo snd chn
*** write voice-synth
*** TODO try different radius values for VOICED->UNVOICED
** TODO [#C] add emacspeak support to eieio-custom-mode
** TODO [#C] Context-sensitive help/suggestions/buttons
*** TODO Check cell to left of selected cell for property name
** TODO [#C] Track wizard
** TODO [#C] write PAD-MATCH-SYNTH 
** TODO [#C] allow analyzing structure based on filtered version of song (lowpass, highpass, bandpass) to capture bass from one and treble/mids from another etc
** TODO [#A] wrap emms-mpv with  --lavfi-complex=
** TODO [#C] S7: consider removing epsilon factor from expsrc*
** TODO write TRIM-MATCH-SYNTH to truncate or pad as needed
** TODO [#B] Import 100 GB music and cache structural analyses
** TODO [#A] Improve SHIFT/STRETCH/SCREW
*** TODO implement shuffle-16
*** TODO try durango-8
** TODO [#A] connect looper gui to looper with proper checkboxes / volumes 
*** TODO first row of looper template can be column header comments
*** TODO vol column should be column 0 of looper
*** TODO should only rewrite first N rows of wav files/waveforms when rendering
*** TODO should not rewrite vol column
*** TODO allow replacing vol button with number
*** TODO mouse-2 fader knob?
*** TODO show length of waveform by changing width of png (mosaic-waveform-pixels-per-minute
*** TODO rework mosaic-populate-looper to use cell-sexp functions and new checkbox class
*** TODO new checkbox class that can toggle any slot-value of an object t or nil
*** TODO checkboxes should set corresponding track play-p
*** TODO easy-import stomper and chopper stuff into looper
*** TODO make transport buttons work with ecasound engine for audition in looper
*** TODO mosaic-render* on looper should not start transport, just config tracks
** TODO [#C] page-icon function to create new blank sheet of whatever type, from template
** TODO [#C] mosaic-render should have an :around method providing status/error
*** TODO refactor per-subclass stuff into mosaic-render :around method
** TODO [#C] Function to toggle borders/headers and test those
** TODO [#C] upgrade to emacspeak 51 and test out tabs
** TODO [#C] show hyperline with transport  (alternate header-line) on right click
*** TODO [#C] use ecasound modeline updater strategy
** TODO [#C] allow saving of stomper patterns to lisp data, re-rendering as wav
** TODO [#C] BROWSER: freesound.org integration
*** TODO audition low quality from browser
*** TODO click next/previous pages in browser
*** TODO import high quality from browser
*** TODO descriptor search
*** TODO combined search
*** TODO transfer sounds from browser->speedbar and speedbar->chopper?
*** TODO call delete-instance on all sounds when changing projects
*** TODO save metadata in .sound-file-name.eieio
** TODO [#C] auto show snd scheme buffer at more relevant times
** TODO [#C] Menu for inserting music symbols
*** TODO pop up palette
*** TODO pop up right click music insertable chars
** TODO [#C] beat/snare detection 
** TODO [#C] add "show clipping" and edit-menu.scm menu options and more
** TODO [#C] show real Windows homedir after any output
** TODO [#C] add eieio custom fields for MOSAIC-SOUND class
** TODO [#C] bind XF86 transport/vol keys
** TODO [#C] show play pos in cell-mode with fringe arrow 
*** TODO annotate sequencer with per-row time stamps
** TODO [#C] export as mp3
** TODO [#C] make theme-howto or script for making sure emacs buttons/menus/dialogs match dark theme
*** TODO allow checking for mate-control-center, mate-settings-daemon, invoking it if found, message if not
*** TODO find exact package list to make themes work
*** TODO use --quiet and --packages arguments to Cygwin-setup.exe so as to do install in background
*** TODO snarf Cygwin setup EXE during mosaic install 
** TODO [#C] use padsp to get pulse support for ecasound on windows?
** TODO [#C] add soundfont support with midi.el and fluidsynth
** TODO [#C] Make waveform renders happen in background
** TODO [#C] improve cell-mode toolbar
** TODO [#B] commands to transpose notes in region (octave, raw, scale)
** TODO [#C] check out http://beta.ccmixter.org/stems
** TODO [#C] implement a function to render/play just the selected region
** TODO [#C] make emacs shortcut on desktop on windows
** TODO [#C] support Soundfont synthesis
** TODO fix enveloper to use STRETCH-ENVELOPE
** TODO [#A] integrate mountain melody
** TODO [#A] glissando: implement interval->radians converter 
** TODO [#A] pizzicato
** TODO [#A] lessen brightness when in lower volumes/registers
** TODO [#A] Fix incorrect measure length/position when tiny notes are used
** TODO [#B] apassionata
** TODO [#B] Improve legato envelopes http://www.s-systems-inc.com/pubs/Strawn_jaes_analysis.pdf
*** TODO [#B] "-𝆪" legato with bow change (amplitude dip is deeper, time gap wider) on seconds, barely so on thirds
*** TODO [#B] legato (no bow change)
** TODO [#C] in final render, render files with one big notr list without reverb and apply during mixing
** TODO [#C] is global amp-env killing noise attack? 
** TODO [#C] sharpen higher partials during attack?
** TODO [#C] trim any clicks from verbed output
** TODO [#C] Enable emacs playing music keys or current cell from a set of samples (one for each pitch)
** TODO [#C] open envelope editor for a spreadsheet cell (hide content unless edit)
*** TODO tiny sparkline image preview in cell using elisp env interp plus gnu plot
*** TODO ENVED-DIALOG, START-ENVELOPING after MOSAIC to interactive mixdown?
** TODO [#C] reuse blank cells in cell mode rendering, avoid propertizing
** TODO [#C] upgrade to new audacity
** TODO [#B] allow easily toggling reverb
** TODO [#B] tremolo
** TODO [#B] sul ponticello
** TODO [#A] more humanization: pitch, timing, timbre, dynamics, rubato
** TODO [#B] analyze spectra of shoe squeaks
** TODO [#B] TWO-TAB to interpolate spectra
** TODO [#B] render evolving note clouds
** TODO [#B] ornaments on scale degrees
** TODO [#C] reuse env array on scheme side to speed up?
** TODO [#C] Allow specifying portamento to note
** TODO [#C] Accelerando / ritardando / time scaling
** TODO [#C] address issue of reverb falloff to absolute silence during a long rest
** TODO [#C] position four players in a space
** TODO [#C] compress scheme output by emitting table of partials at beginning and using numbers to refer to that
** TODO [#C] Or use polyshape as carrier and modulate with NCOS https://ccrma.stanford.edu/software/snd/snd/sndclm.html#ncosdoc
** TODO [#C] PQW for index enveloping?
** TODO [#C] make speedbar optional
** TODO [#C] allow choosing "play" program in mosaic-play-engine-output
** TODO [#C] fix render-synth-chain doesn't call ecasound
** TODO [#C] fix race condition with ecasound---wait until snd finished somehow
*** TODO close all sounds?
** TODO [#C] Try combining music flags to approximate Makamlar note symbols
** TODO [#C] Render-sequence-and-play as one step
** TODO [#C] Uniform command to render sheets of various types
** TODO [#C] try pretty-printing and/or comments to workaround apparent single-shot message length limitation in emacs->snd interface
** TODO [#C] allow typing values directly in cells with overwrite
** TODO [#C] allow column labels
** TODO [#C] Save borders/headers properties in file
** TODO [#C] Begin documenting Emacs API
** TODO [#C] add more status output.  Special mosaic buffer?
** TODO [#B] Allow each synth to specify which properties it will match on (as slot?)
** TODO [#B] define grooves, segues between grooves, song structure
** TODO [#B] generating beats via emphasis  / rhythms 
** TODO [#C] SchemeMosaic Composer generate rhythm loops
** TODO [#C] speedbar support for scheme-mosaic, project browser etc
** TODO [#C] make flycheck work with snd-scheme lint
** TODO [#C] check out new svg images library
** TODO [#C] support IMAGE-SCALING-FACTOR
** TODO [#C] use line number mode to speed up rendering
** TODO [#C] make sure cell-tap and cell-alternate-tap work
*** TODO implement right mouse click as execute
** TODO [#C] mosaic-uninsinuate-speedbar
** TODO [#C] use header line and fringe bitmaps to indicate pos
a** DONE follow emacs naming conventions
   CLOSED: [2019-09-15 Sun 14:39]
** TODO [#C] Fix byte-compilation warnings 
** TODO [#C] Detect offset of first beat
** TODO [#C] Trim to first beat during initial scan
** TODO [#C] store segmentation info and region properties in .snd.scm file for database.
** TODO [#C] compute concatenation score
** TODO [#C] Store database as .snd files plus .snd.scm
*** TODO sound-properties, sound-property, save-state-ignore
** TODO [#C] MIDI-KBD http://elpa.gnu.org/packages/midi-kbd.html

* Issues
** TODO Fix hang when MIX-PAIRWISE or MIX-SYNTHS , try (set! (with-file-monitor) #f)

* Idea bucket
** TODO TWO-TAB interpolates spectra
** TODO CROSS-SYNTHESIS
** TODO [#B] LADSPA
** TODO [#C] TWO-TAB for interpolating spectra
** TODO [#B] NORMALIZE-SOUND
** TODO CLEAN-SOUND, CLEAN-CHANNEL
** TODO [#A] FILTER-SOUND
** TODO [#A] SAVE-STATE
** TODO [#A] SAVE-MARKS
** TODO [#A] SAVE-MARK-PROPERTIES and save-in-header, or use FOO.snd.scm
** TODO [#A] REMOVE-CLICKS and REMOVE-POPS	
** TODO [#B] CHANNEL-CLIPPED? and FIND-CLICK for detecting errors
** TODO [#B] ->FREQUENCY note names and equal temperament
** TODO [#B] adding sounds and dragging mixes
** TODO [#B] CROSS-FADE, DISSOLVE-FADE
** TODO [#B] ZECHO
** TODO [#B] FREEVERB
** TODO [#B] GRANI
** TODO [#B] PLACE-SOUND for panning
** TODO [#C] Investigate PINS instrument for spectral modeling synthesis
** TODO [#C] CONTRAST-ENHANCEMENT via CONTRAST-SOUND
** TODO [#C] CHORDALIZE
** TODO [#C] MUSGLYPHS.scm
** TODO place marks on desired source sections as remixing tool in snd (use saved state)
** TODO mosaic-loopers
* TODO try adding small kickdrum on beat (and/or snare) to help Mixxx bpm detector
* TODO specification language for transformations

** TODO [#C] fix MIDI input on ms-windows
** TODO [#C] fix Windows version doesn't display proper fringe icons for long lines
** TODO [#C] fix music symbol keybindings for Windows
*** TODO fix note entry keybindings for Windows
*** TODO test out Control+Windows key on windows
*** TODO add symbol/note insertion menu
