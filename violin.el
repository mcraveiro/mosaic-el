(defclass violin (player)
  ((type :initform :violin :initarg :type :accessor violin-type)))

(defclass cello (violin)
  ((type :initform :cello :initarg :type :accessor violin-type)))

(defmethod violin-use-partials-p ((v violin)) nil)
(defmethod violin-use-partials-p ((c cello)) nil)

(defmethod violin-vibrato-parameters ((v violin))
  (list :periodic-vibrato-rate 5.0
        :periodic-vibrato-amplitude 0.0025
        :random-vibrato-rate 16.0
        :random-vibrato-amplitude 0.005))

(defmethod violin-main-fm-index ((v violin) freq volume)
  (if (player-pizzicato-p v)
      0.356
    (+ 1.0 (if (player-accent-p v)
               0.5
             0.0))))

(defmethod violin-main-fm-index-parameters ((v violin) freq volume)
  (list :fm-index (violin-main-fm-index v freq volume)))

(defmethod violin-amp-env-parameters ((v violin))
  (list :amp-env (cond ((player-accent-p v)
                        '(0 0  13 1  75 1 100 0))
                       ((player-pizzicato-p v)
                        '(0 0 1 1 10 0.8 15 0.1 100 0))
                       ((player-legato-p v)
                        '(0 0 1 1 10 0.5 50 1.0 99 0.8 100 0.7))
                       (t '(0 0  25 1  75 1  100 0)))))

(defmethod violin-reverb-parameters ((v violin))
  (list :reverb-amount 0.05))

(defmethod violin-noise-parameters ((v violin))
  (list :amp-noise-amount 0.0
        :amp-noise-freq 20.0
        :noise-amount (if (player-pizzicato-p v) 0.0052 0.0)
        :noise-freq 1000
        :ind-noise-amount (if (player-accent-p v)
                              0.1 0.0)
        :ind-noise-freq 20.0))

(defmethod violin-attack-parameters ((v violin) index freq)
  ;;(when (violin-accent-p v)
    (list :attack-index-env (list 0 (1+ index) 1 index)
          :attack-noise-freq 2000.0
          :attack-noise-amp-env (list 0 1 1 0)
          :attack-noise-env (list 0 (* 0.2 freq) 1 0)))

(defmethod violin-fm-rate ((v violin) freq n)
  (ecase n
    (1 1.0)
    (2 3.0)
    (3 4.0)))

(defmethod violin-fm-env ((v violin) freq n)
  (ecase n
    (1 (if (player-accent-p v)
           '(0 1.5 7 .4 75 .6 100 0)
           '(0 1  25 .4  75 .6  100 0)))
    (2 (if (player-accent-p v)
           '(0 1.5 7 .4 75 .6 100 0)
         '(0 1  25 .4  75 .6  100 0)))
    (3 (if (player-accent-p v)
           '(0 4 7 .4 75 .6 100 0)
         '(0 1  25 .4  75 .6  100 0)))))

;; This is adapted from v.scm in Snd sources
(defmethod violin-fm-index ((v violin) main-index freq n)
  (let ((vln (not (eq :cello (violin-type v)))))
    (* (if (player-pizzicato-p v) 0.8
         (if (player-accent-p v) 1.3 1.0))
       (let ((maxdev (* (hz->radians freq) main-index)))
         (ecase n
           (1 (min pi (* maxdev (/ (if vln 5.0 7.5) (log freq)))))
           (2 (min pi (* maxdev 3.0 (if vln (/ (- 8.5 (log freq))
                                               (+ 3.0 (* freq 0.001)))
                                      (/ 15.0 (sqrt freq))))))
           (3 (min pi (* maxdev (/ (if vln 4.0 8.0) (sqrt freq))))))))))

(defmethod player-note-parameters ((v violin) freq volume)
  (let ((main-index (violin-main-fm-index v freq volume)))
    (append
     (violin-amp-env-parameters v)
     (violin-main-fm-index-parameters v freq volume)
     (violin-vibrato-parameters v)
     (violin-reverb-parameters v)
     (violin-noise-parameters v)
     ;; (when (violin-use-partials-p v)
     ;;   (list :partials (mosaic-pitch->partials freq)))
     ;; (violin-attack-parameters v main-index freq)
     (list :fm1-rat (violin-fm-rate v freq 1)
           :fm2-rat (violin-fm-rate v freq 2)
           :fm3-rat (violin-fm-rate v freq 3)
           :fm1-env (violin-fm-env v freq 1)
           :fm2-env (violin-fm-env v freq 2)
           :fm3-env (violin-fm-env v freq 3)
           :fm1-index (violin-fm-index v main-index freq 1)
           :fm2-index (violin-fm-index v main-index freq 2)
           :fm3-index (violin-fm-index v main-index freq 3)))))
