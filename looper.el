;;; mosaic-looper.el --- looper for ecasound         -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  David O'Toole

;; Author: David O'Toole <dto@nomad>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(defclass mosaic-looper (mosaic-chopper) ())

(cl-defmethod mosaic-process-sheet ((sheet mosaic-looper))
  (let ((rows (cell-collect-cells-by-row sheet)))
    nil))

(cl-defmethod mosaic-create-resource-tracks ((self mosaic-engine) num-files &optional play-column)
  (with-mosaic-engine self
    (let ((files (mosaic-resource-files num-files))
          (n 0)
          (play-p* play-column))
      (dolist (file files)
        (when (or (null play-column)
                  (pop play-p*))
          (mosaic-add-track self
                            (make-instance 'mosaic-track
                                           :name (mosaic-resource-file-name n)
                                           :input `(:loop ,(mosaic-session-file
                                                            (mosaic-resource-file-name n))))))
        (cl-incf n)))
    (mosaic-engine-apply-settings self)))

(defvar mosaic-looper-output-files ())

(defun mosaic-populate-looper* ()
  (interactive)
  (let* ((looper (mosaic-find-sheet mosaic-looper-file))
         (tracks (slot-value mosaic-engine-object 'tracks))
         (num-tracks (length tracks))
         (files (mapcar (lambda (track) (slot-value track 'name))
                        tracks))
         (rows ()))
    (cell-sheet-blank cell-current-sheet (+ num-tracks 1) 8)
    (cell-with-current-cell-sheet
     (mosaic-find-all-waveforms :files files :force t)
     (dotimes (n num-tracks)
       (push `(,(format "\"%s\"" (nth n files))
               (cell-image-cell :value
                                ,(format "\"%s\""
                                         (mosaic-session-file (mosaic-waveform-image-file (nth n files))))))
             rows))
     (push `("[cell-sheet-class mosaic-looper]")
           rows)
     (cell-copy-cells-from-sexps (nreverse rows))
     (cell-sheet-move-bob)
     (cell-sheet-paste)
     (dolist (cell (cell-collect-cells cell-current-sheet))
       (mosaic-fontify-cell cell))
     (clear-image-cache)
     (cell-sheet-update)
     (cell-sheet-after-open-hook looper)
     )))

(cl-defun mosaic-mixdown (&optional (files mosaic-looper-output-files))
  "Create a dynamic mixdown in the Snd editor with FILES.
By default this uses `mosaic-looper-output-files'."
  (interactive)
  (mosaic-close-all-sounds)
  (mosaic-new-sound)
  (mapc #'mosaic-mix-sound (mapcar #'expand-file-name files))
  (mosaic-tell-scheme
   `(begin (mosaic-enable-beat-grid)
           (enable-snap-mix-to-beat)
           (enable-snap-mark-to-beat)
           (set! (show-grid) t)
           (arrange-mixes)))
  (mosaic-show-scheme-dwim))

(defun mosaic-populate-looper ()
  (interactive)
  (mosaic-populate-looper*))
  ;; (dolist (cell (cell-collect-cells cell-current-sheet))
  ;;   (cell-set-value cell (cell-find-value cell))))

(provide 'mosaic-looper)
;;; mosaic-looper.el ends here
