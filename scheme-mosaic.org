P#+TITLE: SchemeMosaic: a free system for musical concatenative synthesis
#+AUTHOR: David O'Toole <dto@xelf.me>
#+OPTIONS: toc:3 *:nil num:nil
# #+INFOJS_OPT: path:http://orgmode.org/org-info.js
# #+INFOJS_OPT: view:info mouse:underline up:xelf.html home:http://xelf.me toc:t ftoc:t ltoc:t

* Description

SchemeMosaic is a digital music mashup tool inspired by Steven Hazel's
[[http://awesame.org/soundmosaic/][soundmosaic]]. The main technique employed is concatenative
synthesis. You can find out more about that [[https://en.wikipedia.org/wiki/Concatenative_synthesis][on wikipedia.]]

SchemeMosaic is written in S7 Scheme as an extension to the [[https://ccrma.stanford.edu/software/snd/][Snd
editor]], and is released under the GPL Version 3. Right now
SchemeMosaic is under construction, and only tested with Snd 20.9 and
greater. (Snd itself is distributed under an MIT-like license.)

The goal is to reimplement soundmosaic with improved sound quality,
new features, and musical intelligence so that it can slice and
dice beats and remap tones (and, possibly someday, morph songs into
each other with the use of Autotalent and other algorithms.)

SchemeMosaic is designed to assist with making various kinds of
electronic music, such as so-called chopped and screwed or
vaporwave music. When you run SchemeMosaic, the input file is
sliced into (usually) beat-sized regions; these beat-slices are
then subject to chopping (rearrangement, repetition, skipping) and
screwing (pitch-shifting, time-stretching, and other effects.) With
various helper functions, SchemeMosaic will automatically chop and
screw according to your specifications. This can include arbitrary
Scheme code with the full power of Snd at your disposal.

Here is an Emacs image-dired view of a database:

file:snippets.png

The current version has GUI support via Emacs, for fully interactive
chop-and-screw sessions. The goal is a kind of exploratory
concatenative synthesis. Output can be saved to WAV or FLAC for use
with other applications such as Audacity, Mixxx, LMMS, Ardour, and so
on.

SchemeMosaic can use Soundtouch to guess the tempo (beats per minute)
of a song. The soundstretch command line program must be installed. If
you can't get that working, or if the result is inaccurate, try out
the BPM detection in [[http://mixxx.org][Mixxx]] and enter the number it reports into
SchemeMosaic.

* News

2021-01-13: I'm pleased to announce the official 1.7 release of
SchemeMosaic. Please see the [[http://xelf.me/guide.html][Installation Guide]] for instructions.
  
2021-01-12: Beatboxing has improved. [[file:bb-test-improvements.mp3][File 1]]
  
2021-01-11: More neural network beats: [[file:beats-24-simple.mp3][File 1]] [[file:beats-25.mp3][File 2]]
  
2021-01-10: A neural network trained on beatloops generates [[file:beats-19.mp3][beats.]]
  
2021-01-05: SchemeMosaic can now use a neural network to guess whether
a given sound slice contains a kick, snare, or cymbal. See this video:
[[http://xelf.me/scheme-mosaic-beat-slicer-wizard-demo-2.mkv][MKV file]] or [[https://www.youtube.com/watch?v=aa_ntQGECg4][YouTube]]. I plan to include more types of percussion and
other tasks for the neural network soon, and this is a very exciting
new portion of the work.
  
2021-01-02: Here is [[file:slicer-3.png][a screenshot]] of SchemeMosaic running with the
snazzy "Nano" config.
  
2021-01-02: [[file:slicer-2.png][New screenshot]] of improved Beat Slicer Wizard with new
features.
  
2021-01-01: New video of Beat Slicer Wizard: [[file:scheme-mosaic-beat-slicer-wizard-demo-1.mkv][MKV file]] or [[https://www.youtube.com/watch?v=4E6-nVQ28BQ][YouTube]].
  
2020-12-30: I'm working on a new Beat Slicer Wizard. [[file:./slicer.png][Click for screenshot.]]
  
2020-12-29: New audio experiments: [[file:scanners-ex.mp3][1]] [[file:cool-loops-2.mp3][2]] [[file:thetamax.mp3][3]]. I've also added functions
that can use XMP to split an XM song into separate tracks and import
them at the proper BPM, soon to be wrapped in a nice "Module wizard"
that should help SchemeMosaic interoperate with the world of
Module tracking.
  
2020-12-27: I'm working on a Beatbox wizard, where you beatbox into
the mic with your mouth and SchemeMosaic attempts to come up with a
sampled beat to match. It's a very crude proof of concept, but have a
listen to [[file:beatbox-demo-2.mp3][this two-part mp3]] that begins with me beatboxing and then
switches to a resynthesized version matched from a database of sliced
drum loops. Here is also [[file:beatbox-demo-4.mp3][another demo.]] And [[file:beatbox-demo-5.mp3][another]].
  
2020-12-26: I've added various "Wizards" to help with getting started
and doing common tasks. Here are the screenshots: [[file:wizards-1.png][Wizards 1]] [[file:wizards-2.png][Wizards 2]]
  
2020-12-24: A Merry Witch-House Christmas to you! Here is the latest
content from the world of SchemeMosaic:

 - http://xelf.me/resource-E1.mp3 An old Farbrausch demoscene beat
   chopped-and-screwed with auto-matched "Witch-House" music sliced
   from dozens of tracks. Pretty wild results!
 - New demo video: [[https://www.youtube.com/watch?v=VQbI8zMMrww][YouTube video]] or [[http://xelf.me/scheme-mosaic-demo-2020-2.mkv][MKV file]]
 - Other recent audio: http://xelf.me/scheme-mosaic-2020-2.mp3
 - Older example: an [[http://xelf.me/coherent-4.mp3][audio sample]] of SchemeMosaic automatically
   matching vocal and guitar elements to a backing track that is also
   chopped and screwed, with a bit of postprocessing in Mixxx and
   Audacity.

file:./screenshot-thumbnail.png

 - [[file:screenshot.png][(Click for full view)]] A nice screenshot of the Chopper/Looper
   interfaces. You can now dynamically switch databases on the fly
   during a session by specifying the database folder in the
   approprate spreadsheet cell. You can loop the resulting files with
   Ecasound, or open them in Audacity. More to come!

* Previous videos

 - https://www.youtube.com/playlist?list=PL0nuOl15T_6X0YL6mMBfm8zBSTmDpaRxh

* Licensing

EmacsMosaic and SchemeMosaic are Free Software released under the
terms of the GNU General Public License, Version 3. You can find a
copy included in the Mosaic project directory, in a file called
LICENSE. You can also find the full text at:

 - https://www.gnu.org/licenses/gpl-3.0.html

Several Emacs Lisp library components are also included. These are:

  - midi.el, jack.el, and ecasound.el (by Mario Lang, GPL)
  - ladspa.el (by T.V. Raman, GPL)
  - inf-snd.el (by Michel Scholz, MIT License)
  - cell.el (by David O'Toole, GPL)

See each file for full copyright and licensing information.

Audio functionality is built on Bill Schottstaedt's Snd editor and the
Ecasound HDR application. The Mosaic project installation includes
full licensing information and sources for the included Emacs Lisp
components; on platforms where Ecasound and Snd are shipped alongside
Mosaic, full sources are included for the applications themselves as
well. Please see their respective websites for more information.

 - http://nosignal.fi/ecasound/ GPL.
 - https://ccrma.stanford.edu/software/snd/snd/snd.html Snd is
   distributed under an MIT-like license.

On Windows systems, Mosaic works with Cygwin and a number of its
packages. See http://cygwin.org/ for full information on Cygwin.

The `defun-memo' facility is based on code by Peter Norvig
for his book "Paradigms of Artificial Intelligence
Programming". The modified version is redistributed here.

You can find more information on Norvig's book, and the full license
for the `defun-memo' code, at his website:

 - http://www.norvig.com/paip.html
 - http://www.norvig.com/license.html

The neural network implementation is in the public domain, by Scott
E. Fahlman for Carnegie-Mellon University.
 
* Installation

See http://xelf.me/guide.html for GNU/Linux and Windows 10 install
instructions.

Please see also the Emacs side documentation: http://xelf.me/emacs-mosaic.html

* Source code repository

The source code may be found here: https://gitlab.com/dto/mosaic

* Overview of usage

This document covers SchemeMosaic internals. Check out
http://xelf.me/emacs-mosaic.html for information on using SchemeMosaic
through Emacs.

The sound slices that SchemeMosaic processes are called
regions. SchemeMosaic keeps a special hash table where a set of
properties for each region are recorded. Each property list is the
descriptor of its region. As stored in the hash table
*REGION->PROPERTIES*, these data together with the corresponding
regions constitute the database being used for concatenative
synthesis. The descriptors are the keys used to search for sounds in
the database. You can match against the entire database, or any subset
at a time.

The objects that generate sound mosaics are called synths. The main
way of getting things done is to call GENERATE-REGIONS on synths of
various synth classes, and mix, match, and chain them. SPLICE-SYNTHS
or other functions can be used to combine synths. 

The object system used by SchemeMosaic is the one included with Snd,
defined in stuff.scm and s7test.scm, plus a few tweaks of my
own. To avoid conflicts with other programs, I append a caret to the
names of the main OO symbols so that you use DEFINE-CLASS^,
DEFINE-METHOD^, and MAKE-INSTANCE^. The system it implements is a
simplified CLOS.

To find your project files easily, you may wish to set
(MOSAIC-PROJECT-DIRECTORY) to something like
/home/username/myproject/ and then make calls to (PROJECT-FILE
FILENAME).

Further below is a dictionary for all the important elements of
SchemeMosaic. To get an overview of how things work, check out the
following entries:

 - SYNTH - Base class for SchemeMosaic sound generators.
 - MOSAIC-SYNTH - Chop'n'screw utility class.
 - SHIFT-MATCH-SYNTH - Pitch-shift near-matches (or not-so-near matches) to the target.
 - STRETCH-MATCH-SYNTH - Time-stretch different length regions to the target length.
 - SEARCH-MATCH-SYNTH - Search databases and match snippets to target.
 - FILE-SYNTH - Load any sound file into a new synth.
 - MAKE-INSTANCE^ - Make a new object of any class.
 - MOSAIC-PROJECT-DIRECTORY - Where SchemeMosaic looks for files.
 - PROJECT-FILE - How to look for files.
 - REMEMBER-REGION-PROPERTIES - Selecting what you want to match.
 - MATCH-PROPERTIES - Finding regions that matching properties exactly.
 - FUZZY-MATCH-PROPERTIES - Matching regions more flexibly.
 - MATCH-REGIONS - Matching up regions whose properties match.
 - SHIFT-MATCH-REGIONS - Pitch shifting to correct suboptimal matches.
 - SHUFFLE - Rearranging and processing groups of regions.
 - GENERATE-REGIONS - The synth computes its output.
 - FIND-REGIONS - Cache output on demand.
 - MERGE-SOUND - Concatenate regions into a single sound.
 - SYNTH-SOUND - Concatenate a synth's output into a single sound.
 - MIX-SOUNDS - Mix two sounds.
 - CHAIN-SYNTHS - Connect the input of one synth to the output of another.
 - SPLICE-SYNTHS - Use sound from one synth and structure from another.

** Differences between SchemeMosaic and the original soundmosaic

 - SchemeMosaic, by default, applies click reduction when merging
   fragments. 
 - SchemeMosaic does not scale source regions to match the amplitude
   of the target region. This may be added as an option so that
   results can follow the amplitude contour of the target if desired,
   but doing it by default can cause problems with a cappella
   vocals. If a quiet area is matched to a louder one, soundmosaic
   will scale it up. When this is applied to the near-silences between
   words in an a cappella vocal track, the result is bursts of loud
   noise in between better matches.
 - SchemeMosaic can pitch-shift suboptimal pitch matches, and has
   many other new features.

** Known issues

 - Time-stretching needs improvement. It isn't terrible, but sounds
   too reverberated for my taste. Some "new improved"
   stretching/shifting functions are under construction, and are named
   STRETCH-REGION* and SHIFT-REGION* instead of SHIFT-REGION and
   STRETCH-REGION. SchemeMosaic will try to offer several options in
   each category while choosing reasonable defaults.
 - Some visual glitches in cell-mode when displaying images in cells.

* Dictionary

Below is a dictionary of documentation strings extracted from
SchemeMosaic. Be sure to check also the [[http://xelf.me/emacs-mosaic.html][Emacs side]].
  
** synth 
 SYNTH is the SchemeMosaic base class for
concatenative synthesis generators. Each synth object is both a
consumer and producer of regions. The following slots are available to
initialize with keyword arguments during MAKE-INSTANCE^:

 - TARGET-REGIONS, the sequence of regions you are trying to
   reconstruct or emulate.  
 - SOURCE-REGIONS, the sequence of regions to
   choose from during matching against the TARGET-REGIONS. The output
   will be composed of these, or some transformation thereof.
 - RANDOM-MATCH, telling the synth whether to randomly choose one match
   whenever multiple simultaneous matches occur. By default this is 
   set to #t. When this is #f instead, the first match is always chosen.
 - PARAMS-FN, a function to compute the descriptors. This is optional
   and by default is REGION->DESCRIPTOR, which caches commonly used 
   properties.

During initialization of a synth object, the following slots are
filled automatically based on the parameters given to MAKE-INSTANCE^:

 - DESCRIPTORS, the sequence of property alists corresponding to each 
   of the TARGET-REGIONS.
 - GRAMMAR, a set of production rules derived from structural analysis
   of the DESCRIPTORS.
 - AXIOM, a mixed sequence containing both variables and descriptors, 
   which expands back into DESCRIPTORS when passed to GENERATE-PHRASE.

Aside from INITIALIZE-INSTANCE, the main way of using a synth after
creation is to call its GENERATE-REGIONS (or FIND-REGIONS) method with
a suitable phrase (i.e. a suitable sequence of descriptors that the
synth will attempt to match in its output.) 


** file-synth 
 Load and slice a file without further processing at this stage.

** mosaic-synth 
 MOSAIC-SYNTH is a utility class for chopping, screwing,
and matching multiple elements to a backing track that may itself also
be chopped, screwed, etc. 

The following keywords are available during initialization with
MAKE-INSTANCE^:

  - INPUT-FILE, the file to read and slice.
  - OUTPUT-FILE, the file to write. By default this is (MOSAIC-TEMP-FILE).
  - TEMPO, the number of beats per minute of the INPUT-FILE.
    The default value #f means to use FIND-TEMPO.
  - SLICE-SIZE, the length in seconds of each slice. The default is
    a quarter note (relative to the TEMPO).
  - MEASURE-LENGTH, number of beats per measure. By default this is 4.
  - PROCESSOR, a function of one argument (a list of regions to
    operate on). It can rearrange and transform the regions
    (nondestructively of course) into new output regions. See SHUFFLE
    and SHUFFLE-8.
  - SRATE, the sampling rate.

After initialization, the following fields will be filled:

  - INPUT-REGIONS, the sliced input file.
  - OUTPUT-REGIONS, the resulting mosaic.
  - SOUND, the resulting output sound. It will be configured to 
    display beats/measures on the X axis, and you can use 
      (ENABLE-SNAP-MARK-TO-BEAT)
    to position marks on beats in Snd. See also:
    DISABLE-SNAP-MARK-TO-BEAT, ENABLE-SNAP-MIX-TO-BEAT.
  - SOURCE-REGIONS, the same as INPUT-REGIONS.
  - TARGET-REGIONS, the same as OUTPUT-REGIONS.
 
These last two are for chaining mosaic-synths with other synths.
See also CHAIN-SYNTHS and SPLICE-SYNTHS.

Unlike other SYNTH classes, all the processing is done during
INITIALIZE-INSTANCE. GENERATE-REGIONS just returns the mosaic-synth's
OUTPUT-REGIONS.

** search-match-synth 
 Configurably match target regions to a
specified database. See the documentation
for `(INITIALIZE-INSTANCE (SMS SEARCH-MATCH-SYNTH)) for more
information on how to use this class.

** shift-match-synth 
 Pitch-shift each SOURCE-REGION to match
its TARGET region. Use the TOLERANCE argument (in cycles per second)
to control sensitivity of shifting.

** stretch-match-synth 
 Time-stretch each SOURCE-REGION to
match the length of its corresponding TARGET-REGION.

** (all-suffixes symbol seq) 
 

** (annotate property region (value #t)) 
 

** (annotate-up-and-down-beats-8 a b c d e f g h) 
 

** (append-symbol s (seq *sequence*)) 
 

** (append-trailing-slash string) 
 Return a copy of STRING with a slash appended.

** (arrange-mixes) 
 Spread out mixes vertically so they don't overlap.

** (centroid-distance a b) 
 

** (chain :rest ops*) 
 

** (chain-synths (s1 synth) s2 (phrase #f)) 
 Make a copy of S1, but with the generated regions of S2 spliced
in as the SOURCE-REGIONS (or concatenated as INPUT-FILE, in the case
of mosaic-synths) of S1. INITIALIZE-INSTANCE is called on the new
synth. PHRASE is passed through to GENERATE-REGIONS when obtaining the
output of S2.
** (chop (pattern vapor-8)) 
 

** (cleanup) 
 Free up resources.

** (clear-resources) 
 

** (close-all-sounds) 
 Close all Snd sounds.

** (close-other-sounds sound) 
 Close all sounds except SOUND.

** (close-other-sounds* sound*) 
 Close all sounds except those in the list SOUND*.

** (convert-rule simple-rule (params-fn (lambda (s) (list s)))) 
 

** (count-uses search-symbol) 
 

** (current-synth?) 
 

** (current-synth) 
 

** *database-directory* 
 Directory to load DAT files from.

** (define-class^ class-name inherited-classes (slots ()) (methods ())) 
 Define a new class.
** (define-method^ (name (instance class) :rest body)) 
 Define a new method.
** (define-selection-via-marks-here) 
 Select the area between the markers surrounding the cursor.

** (del-assoc key lst) 
 

** (delete-last-symbol!) 
 

** (delete-properties deleted-properties source-properties) 
 

** (delete-rule! deleted-symbol) 
 

** (descriptor-properties-only properties) 
 The function DESCRIPTOR-PROPERTIES-ONLY removes floating point
properties from the alist PROPERTIES. A descriptor must contain only
discrete properties to be matched, i.e. no floating point numbers
allowed. 

This function also removes the special POSITION property. Position is
a discrete property, but should be matched through other means.

** (descriptor? x) 
 Return #t if the argument X is a region descriptor.

** (disable-snap-mark-to-beat) 
 Allow marks to be positioned anywhere.

** (disable-snap-mix-to-beat) 
 Allow mixes to be positioned anywhere.

** (echo-sound sound (time 0.25) (scaler 0.4)) 
 Add delay (echo) to a sound. Try using (quarter-note)
or (eighth-note) as values for TIME.

** (eighth-note) 
 

** (emit-org-entries) 
 

** (enable-beat-grid sound tempo measure-length) 
 Set up the properties of SOUND to display musical time.

** (enable-snap-mark-to-beat) 
 Make sure dragged marks always end up on beat boundaries.

** (enable-snap-mix-to-beat) 
 Make sure dragged mixes always end up on beat boundaries.

** (enforce-utility!) 
 

** (ensure-phrase (s synth) phrase) 
 Return a default phrase for this synth if PHRASE is #f.
** (euclidean-distance p q) 
 

** (expand-selection-to-all-channels) 
 Expand the current selection to all channels of the current sound.

** (extend-left-maybe! sym) 
 

** (extend-right-maybe! sym) 
 

** (extract-channel filename snd chn) 
 

** (file->sexp file) 
 Return the Scheme expression previously written to FILE.

** (file->synth file (slice-size (mosaic-slice-size))) 
 Load FILE into a new synth at slice size SLICE-SIZE.

** (file->synth-verbatim file (slice-size (mosaic-slice-size))) 
 Load FILE into a new PASS-THROUGH-SYNTH at slice size SLICE-SIZE.

** (find-axiom grammar) 
 Return the axiom sequence of GRAMMAR.

** (find-beat sound) 
 Attempt to find the sample offset of the first drum beat in SOUND.
This can fail ungracefully; if you wish to disable beat detection,
pass #t for the FIND-OFFSET argument of MOSAIC-IMPORT-SOUND on the
Scheme side, or set `mosaic-import-find-offset' to `nil' on the Emacs
side.

** (find-grammar regions) 
 Return the grammar found by FIND-STRUCTURE when analyzing REGIONS.

** (find-highest-amplitudes region) 
 

** (find-pair seq a b) 
 

** (find-regions (s synth) (phrase #f) (force? #f)) 
 FIND-REGIONS is a caching front end to GENERATE-REGIONS. When you call
FIND-REGIONS for the first time on a synth, the OUTPUT-REGIONS slot
will be filled with the result of GENERATE-REGIONS, and the next time
FIND-REGIONS is called it will return the cached OUTPUT-REGIONS unless
the FORCE? argument is given.

** (find-resource-file index) 
 

** (find-sound-structure regions (show? #f) (properties-fn descriptor-properties-only)) 
 Return the structure Sequitur finds for REGIONS. SHOW? determines
whether to show each step for debugging. PROPERTIES-FN should remove
any properties you don't want to match on. By default this is
DISCRETE-PROPERTIES-ONLY.

** (find-structure seq (reparse? #f) (show? #f)) 
 Return a hash table of production rules characterizing the
structure found by Sequitur upon analyzing SEQUENCE. The argument
SHOW? means whether to display each step; this is used for debugging.
The parameter REPARSE? indicates whether to use oblivious reparsing,
but this is not yet implemented.

** (find-tempo file) 
 Attempt to use Soundtouch to find tempo of FILE.

** (flatten lst) 
 

** (float-vector->region fv) 
 

** (forget-all-region-properties) 
 Clear all cached region properties.

** (forget-all-regions) 
 Forget all Snd regions.

** (forget-region-properties region) 
 Clear the cached properties of REGION.

** (framples* r) 
 

** (fuzzy-match-descriptors descriptor-1 descriptor-2 (match-fn equal?)) 
 

** (fuzzy-match-properties properties regions (match-fn equal?)) 
 Similar to MATCH-PROPERTIES, but matches two regions if any
property value is MATCH-FN between them, whereas regular
MATCH-PROPERTIES requires that all property values match.

** (generate-phrase grammar phrase) 
 Expand PHRASE using the rules in GRAMMAR.

** (generate-regions (s synth) (phrase #f)) 
 Generate a new sound (as a sequence of regions) from the
current synth. Return a sequence of regions matching the sequence of
properties that expands from PHRASE. See also GENERATE-PHRASE and the
functions RESYNTHESIS-TEST-1 and RESYNTHESIS-TEST-2 etc.  If not
supplied with a PHRASE, the synth will choose the axiom (i.e. the
entire structure.) You can use (SHOW-GRAMMAR (MY-SYNTH 'GRAMMAR)) to
get an idea what values to try giving for PHRASE.
** (grammar-rule grammar symbol) 
 

** (half-note) 
 

** (hash->plist hash) 
 

** (identity-regions regions) 
 

** (initialize-instance (sms search-match-synth)) 
 Initialize a SEARCH-MATCH-SYNTH with the following arguments:
 - :TARGET-REGIONS     The regions to be matched.
 - :DATABASE           The session directory where the database is to be loaded from. 
                       The default is the currently loaded database.
 - :SCORE-FUNCTION     This function accepts a region and a UUID and compares their metadata.
                       Choices include SPECTRUM-DISTANCE, CENTROID-DISTANCE, TEMPO-DISTANCE, and WEIGHTED-DISTANCE.
 - :PROPERTIES->QUERY  Control which UUID's get analyzed during search. Use MATCH-ALL to exhaustively match the entire database.
                       The default is a function called PROPERTIES->QUERY, which respects the value of *SEARCH-PROPERTIES*.

** (initialize-instance (s synth)) 
 This method is called after an object is created. As in
any method you define with DEFINE-METHOD^, you can use
(CALL-NEXT-METHOD) within the body to invoke the parent class
method.
** (initialize-mosaic) 
 Set up the Snd environment for SchemeMosaic. This will increase
Snd's maximum region limit to a high number.

** (insert-region* r pos snd chn) 
 

** (join-regions a b) 
 Return a new region that is the concatenation of regions A and B.

** (kodaly-regions (slice-size *kodaly-slice-size*)) 
 

** (last-symbol (seq *sequence*)) 
 

** (lazy-region region-or-uuid) 
 

** (loudest-freqs pairs (n 2)) 
 

** (loudest-notes pairs (n 2)) 
 

** (make-graphable seq) 
 

** (make-instance^ class . args) 
 Create a new object instance of class CLASS with the initialization
keyword argument/value pairs in ARGS.

** (make-region* beg end snd chn) 
 

** (make-uuid) 
 

** (make-variable-name) 
 

** (match-all properties) 
 A function for the SEARCH-MATCH-SYNTH's PROPERTIES->QUERY field
that matches the entire database. Use this to exhaustively match the
entire database; set it to something else to narrow the search and
speed things up.

** (match-and :rest clauses) 
 Return a function that returns #t when all the CLAUSES return #t.

** (match-not clause) 
 Return a function that returns the negation of the CLAUSE.

** (match-or :rest clauses) 
 Return a function that returns #t when any the CLAUSES return #t.

** (match-properties properties regions (match-fn equal?)) 
 Return a list of regions from REGIONS (if any) matching PROPERTIES.
This is also used by GENERATE-REGIONS.  By default, the REGIONS come
from the current synth's SOURCE-REGIONS; if these are empty, then from
TARGET-REGIONS. If this isn't what you want, pass the REGIONS you want
to search instead. MATCH-FN determines the equality test used for
matching descriptors. To match against the entire database instead of
just the contents of this synth, pass (MOSAIC-ALL-REGIONS) as the
REGIONS argument.

** (match-property property value) 
 Return a function matching any one PROPERTY to the value VALUE.

** (match-property-with-tolerance property value tolerance) 
 Return a function matching any one PROPERTY to VALUE, within
TOLERANCE.

** (match-regions score-function target-regions source-regions) 
 Attempt to match each region in TARGET-REGIONS with its \best
match\ in SOURCE-REGIONS. The regions are compared via
SCORE-FUNCTION, which by default is PITCH-DISTANCE. MATCH-REGIONS
returns a list of regions chosen from SOURCE-REGIONS chosen to best
match (in each case) their respective TARGET-REGIONS. See also
MATCH-PROPERTIES and FUZZY-MATCH-PROPERTIES.

** (match-synths s1 s2) 
 Make a new MATCH-SYNTH matching the output of synth S2 to
the output of synth S1.

** (match-tempo-with-tolerance (tolerance 5.0)) 
 

** (merge-pairwise snd chn regions1 regions2) 
 Merge REGIONS-1 and REGIONS-2 pairwise into the sound SND, but
align the beginnings of respective region pairs so that the outcome is
the same as having used MIX-PAIRWISE first and then MERGE-SOUND.

** (merge-sound-as-vectors snd regions) 
 

** (merge-sound-insert^ fn snd (chn 0) regions) 
 

** (merge-sound-insert-raw snd (chn 0) regions) 
 

** (merge-sound-insert snd (chn 0) regions) 
 

** (merge-sound-insert-v vecs chn regions) 
 

** (merge-sound-raw snd regions) 
 Merge a list of REGIONS into one sound, SND. Does not apply click
reduction.

** (merge-sound snd regions) 
 Merge a list of REGIONS into one sound, SND. Applies click
reduction.

** (merge-sounds-v vecs regions) 
 

** (mixdown :rest spec) 
 Mix multiple sounds with amplitudes applied.
Example: (MIXDOWN MY-SOUND-1 0.8 
                  MY-SOUND-2 0.5
                  ...)

** (mixdown-synths :rest spec) 
 Like MIXDOWN, but the sound arguments are synths.

** (mix-pairwise regions1 regions2) 
 Return a list of regions where the first of each set are mixed
together, the second of each set, etc. The output regions are the same
size as the longer of the two regions being mixed at each step.

** (mix-pulse a) 
 

** (mix-regions a b) 
 Return a new region that is the mix of regions A and B.

** (mix-sounds a b) 
 Return a new sound that is the mix of sounds A and B.

** (mix-synths s1 s2) 
 Mix pairwise the output regions of S2 into those of S1,
and return a new synth whose Sequitur structure is retained from
S1 but whose regions are mixed together.

** (mix-synth syn sound name) 
 Mix the output of the synth SYN into the sound SOUND.
You may wish to call ARRANGE-MIXES after several uses of MIX-SYNTH.

** (morph-match-synths target-synth source-synth (tolerance 5.0)) 
 Make a new synth morphing the output of synth SOURCE-SYNTH to the
output of synth TARGET-SYNTH. The regions of SOURCE-SYNTH are matched,
pitch-shifted, and then stretched to the length of the regions of
TARGET-SYNTH. The TOLERANCE argument only affects shift-matching.

** (mosaic-a4-pitch) 
 Master tuning parameter. Pitch of A above middle C.
You can set this with (SET! (MOSAIC-A4-PITCH) VALUE).

** (mosaic-add-entry-to-database entry) 
 Add a piece of slice metadata, the ENTRY, to the current database.

** (mosaic-all-regions) 
 Return a list of all the regions in the database.

** (mosaic-data-file file) 
 Return the metadata file name corresponding to FILE.

** (mosaic-enable-beat-grid) 
 

** (mosaic-import-sound file tempo slice-size find-offset) 
 Slice and analyze FILE at TEMPO with SLICE-SIZE, importing into the
current session's database. Records metadata to FILE.dat in the
current session. If FIND-OFFSET is #t, attempt to find the offset of
the first beat using FIND-BEAT. As FIND-BEAT can fail, the default is
#f. Pass #t (or set `mosaic-import-find-offset' to `t' on the Emacs
side) to turn on beat finding.

** (mosaic-load-database dir) 
 Load all the slice metadata files in DIR into the current
database.

** (mosaic-load-data-file dat-file (dir *mosaic-session-directory*)) 
 Add all the pieces of slice metadata in DAT-FILE to the currently
loaded database table.

** (mosaic-new-database) 
 Create a new database. This only clears the Scheme metadata, and
doesn't modify files.

** (mosaic-output-file) 
 Where to place final SchemeMosaic output for this run.

** (mosaic-pad sound len) 
 

** (mosaic-play-looping file) 
 

** (mosaic-play-sound-file file) 
 

** (mosaic-postprocess-grammar grammar) 
 Replace region properties with their UUID's.
Otherwise the region data gets written to the output file along with
the contents of the hash table.

** (mosaic-project-directory) 
 Base directory for finding project files. See also PROJECT-FILE.
You can set this with (SET! (MOSAIC-PROJECT-DIRECTORY) \/my/path\

** (mosaic-region->snd-region r) 
 Return a SND region version of the Mosaic region R.

** (mosaic-resource index) 
 

** (mosaic-search-database^ func) 
 Call FUNC on each set of properties in the current
database. Returns a list of all slice UUID's for which FUNC returned
#t when given that slice's properties.

** (mosaic-session-directory) 
 Base directory for finding session files. See also SESSION-FILE.
You can set this with (SET! (MOSAIC-SESSION-DIRECTORY) \/my/path\)

** (mosaic-slice-size (tempo *mosaic-beats-per-minute*) (divisor *mosaic-beats-per-measure*)) 
 

** (mosaic-stomp file time) 
 

** (mosaic-switch-to-database database) 
 Switch to the database DATABASE for searches.

** (nonterminal? symbol) 
 

** (normalize-region-properties! region) 
 Rewrite a region's property list so that the alist entries are
sorted alphabetically by property name. This is done to set up
property alists for comparison with EQUAL? and other functions.

** (note-lists-only properties) 
 

** (note->pitch note) 
 Return the pitch of MIDI note NOTE.

** (only-resource-sounds) 
 Close all sounds, and then reopen those in the resource list.

** (org-docstring spec) 
 

** (pair-symbol a b) 
 

** (pair-variable a b) 
 

** (pairwise-difference p q) 
 

** (pairwise-square-of-differences p q) 
 

** (pitch-distance region-1 region-2) 
 

** (pitch->note pitch) 
 Return the MIDI note number closest to PITCH.

** (plist->hash plist) 
 

** (prefixes symbol seq) 
 

** (process^ fn amount) 
 

** (process-sound^ func block-len snd chn) 
 

** (project-file file) 
 Return the full path name of FILE within your project directory.
To access subdirectories you can put slashes in the FILE argument.

** (properties-distance a b) 
 

** (properties->query properties (operator match-or)) 
 

** (pulse-mosaic file bpm) 
 

** (quarter-note) 
 

** (quiet-region region) 
 

** (random-choose lst) 
 

** (record-pair! a b c) 
 

** (reduce-clicks pos len snd chn) 
 Attempt to reduce clicks at edges of concatenated sections.

** (reduce-clicks-region region) 
 

** (region-beat-energy? beg end snd nil) 
 

** (region-beat-power? beg end snd nil) 
 

** (region-centroid* region) 
 

** (region->descriptor r) 
 Default function for mapping a region to its descriptor. Here we
pre-cache spectra, pitch, and note properties by default when regions
are added to a synth.

** (region-loudest-pitch region) 
 

** (region-note* region) 
 

** (region-notes* region) 
 

** (region-pitches* region) 
 

** (region-pitch* region) 
 

** (region-pitch-simple region) 
 

** (region-property property region) 
 Return the value of property PROPERTY for REGION.

** (region-rms* region) 
 

** (region-spectrum* region) 
 

** (region-total-energy* region) 
 

** (remember-region-properties names region) 
 Cache the properties listed in NAMES for REGION. This currently
only works for SPECTRUM, PITCH, NOTE, PITCHES, and NOTES. Of these,
the first three are cached by default anyway upon loading into a
synth; if you want to analyze NOTES etc, do:
  (REMEMBER-REGION-PROPERTIES '(NOTES) MY-REGION) first.

** (remove-duplicates* seq (eq-func? equal?)) 
 

** (remove-duplicates** seq (eq-func? equal?)) 
 

** (reparse! (seq *sequence*)) 
 

** (replace-pair! seq a b c) 
 

** (resynthesis-test-1) 
 

** (resynthesis-test-1a) 
 

** (resynthesis-test-2) 
 

** (resynthesis-test-2a) 
 

** (resynthesis-test-2b) 
 

** (resynthesis-test-2c) 
 

** (rewrite-expansion! expansion symbol definition) 
 

** (rule-expansions g) 
 

** (rule-variable g) 
 

** (save-region* region file) 
 

** (scale-region region (amp 1.0)) 
 Amplify REGION by AMP.

** (scan-sound^ func block-len snd chn (save-position? #t) (offset #f)) 
 Scan through a sound and call (FUNC beg end snd chn) for each block
of BLOCK-LEN seconds in length, collecting the results in a list.

** (scan-until^ func block-len snd chn) 
 Scan through a sound and call (FUNC beg end snd chn) for each block
of BLOCK-LEN seconds in length, until the FUNC returns #t. Returns 
the position in samples of the point at which FUNC returned #t, 
or #f if FUNC never does so.

** (screw (amount vinyl)) 
 

** (screw-region region factor) 
 

** (search-mark-backward pos snd chn) 
 

** (search-mark-forward pos snd chn) 
 

** *search-properties* 
 List of property names to try matching during PROPERTIES->QUERY.

** (second-last-symbol (seq *sequence*)) 
 

** (seen-pair? a b) 
 

** (sequitur-test-1 (reparse? #f) (show? #f)) 
 

** (sequitur-test-2 (reparse? #f) (show? #f)) 
 

** (session-file file (dir *mosaic-session-directory*)) 
 Return the full path name of FILE within your session directory.
To access subdirectories you can put slashes in the FILE argument.

** (set-mosaic-a4-pitch value) 
 

** (set-region-property! property region value) 
 Set the value of PROPERTY in REGION to VALUE.

** (sexp->file sexp file) 
 Write the Scheme expression SEXP as text to the file FILE.

** (shift (amount vinyl)) 
 

** (shift-match-regions target-regions source-regions (tolerance 5.0) (max-shift 50.0)) 
 Works like MATCH-REGIONS, but pitch shifts the best match if the
best match is more than TOLERANCE Hz off the target pitch.

** (shift-match-synths s1 s2 (tolerance 5.0)) 
 Make a new SHIFT-MATCH-SYNTH matching the output of synth S2 to the
output of synth S1.

** (shift-region region factor) 
 Pitch shift REGION by FACTOR.

** (shift-region* region factor) 
 

** (show-grammar grammar) 
 Display the rules of GRAMMAR.

** (show-progress* s data) 
 

** (show-structure (grammar *symbols*)) 
 

** (shuffle-8 regions pattern (amount 1.0)) 
 Like SHUFFLE, but taking eight regions at a time and giving eight
variables: A B C D E F G H.

** (shuffle regions pattern (amount 1.0)) 
 Rearrange the REGIONS according to the PATTERN. Every group of four
regions is bound to variables A B C D and then PATTERN is evaluated to
produce the results. Specify (D C B A) instead to reverse every four
regions, and so on. Because PATTERN is evaluated, you can include
arbitrary code modifying the regions A, B, C, and D in any way you
like, including SHIFT-REGION, STRETCH-REGION, and so on.

** (silent-region region) 
 Return a silent region the same length as REGION.

** (simple-drum) 
 

** (simple-expansion symbol) 
 

** (sixteenth-note) 
 

** (skatter-regions regions) 
 

** (skitter-regions regions) 
 

** (snap-all-marks-to-beat snd chn) 
 Snap all marks to beats in the sound SND, without having to drag
them. This doesn't enable or disable snapping of dragged marks to
beats, it just moves them.

** (snd-region->mosaic-region r) 
 Return a Mosaic region version of the SND region R.

** (song-test base-file vocal-file solo-file) 
 

** (sort-frequencies amps freqs) 
 

** (sound->region! name-or-sound) 
 Convert SOUND (object or filename) to a region. Closes SOUND.

** (special-match* var input) 
 

** (spectrum-distance a b) 
 

** (spectrum-plus-tempo-distance a b) 
 

** (speed-region region factor) 
 

** (splice-synths s1 s2) 
 Make a copy of S1, but with the TARGET-REGIONS of S2 spliced in
as the SOURCE-REGIONS of S1. The new synth keeps S1's class, Sequitur
structure, and other data. See also RESYNTHESIS-TEST-2.

** (split-sound-to-regions block-len snd (chn 0)) 
 Split the sound SND into regions of size SLICE-SIZE seconds in
length.

** (startup) 
 Begin with a clean slate, no sounds open, and all resources cleared
up.

** (stretch (amount (/ 1.0 vinyl))) 
 

** (stretch-match-regions target-regions source-regions (stretch-fn stretch-region*)) 
 Works like MATCH-REGIONS, but stretches the SOURCE-REGIONS to
match the lengths of their corresponding TARGET-REGIONS. Because
SchemeMosaic's stretch functions still need work, the result may be
less than optimal. Try passing STRETCH-REGION* (or your own function)
instead of STRETCH-REGION.

** (stretch-match-synths s1 s2) 
 Make a new STRETCH-MATCH-SYNTH stretching the output of synth S2 to
the output of synth S1.

** (stretch-region region factor) 
 Time stretch REGION by FACTOR.

** (stretch-region* region factor) 
 

** (structure-rules structure) 
 

** (suffixes symbol seq) 
 

** (synth-sound s) 
 Return a new sound composed of the generated regions of the synth
S.

** (takes2-regions (slice-size *kodaly-slice-size*)) 
 

** (tempo-distance a b) 
 

** (terminal? symbol) 
 

** (trailing-slash? string) 
 Return #t if there is a slash at the end of STRING.

** (try-match t score-function source) 
 

** (unlambdify-regions x) 
 

** (useful-rule? search-symbol) 
 

** *uuid->properties* 
 Hash table mapping each slice's UUID to its metadata
properties. This is the current database.

** (uuid->region uuid) 
 Load and return the audio slice data for UUID from UUID.flac

** (vaporwave (pattern vapor-8)) 
 

** (variables) 
 

** (verify-all-terminals phrase) 
 

** (violin-region (start-time 0) duration frequency amplitude) 
 

** (warp region (shift 1.0) (stretch 1.0) (seglen 0.15) (hop 0.05) (ramp 0.01)) 
 

** (weighted-distance (spectrum 1.0) (centroid 1.0) (tempo 0.1)) 
 Returns a SCORE-FUNCTION where SPECTRUM is the coefficient of the
spectrum distance, CENTROID is the coefficient of the centroid
distance, and TEMPO is the coefficient of the tempo distance. The
centroid is a measure of a spectrum's `center of gravity'. Limiting
tempo distance can help avoid excessive use of STRETCH-MATCH-SYNTH.

** (whitney-regions (slice-size *kodaly-slice-size*)) 
 

** (whole-note) 
 

** (write-error-timestamp-file text) 
 

** (write-timestamp-file (n #f)) 
 

