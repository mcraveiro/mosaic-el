(flycheck-define-checker snd-scheme
  "A Snd-scheme syntax checker using the Snd linter."
  :command ("/usr/local/bin/snd"
	    "-b" "/usr/local/share/snd/lint.scm"
	    "-e" (eval (concat "'(begin (lint \""
			       (buffer-file-name (current-buffer))
			       "\") (exit))'"
			       )))
  :error-parser (lambda (output checker buffer)
		  (when (string-match (rx line-start
					  (group (one-or-more (not (any ?\())))
					  "(line " (group (one-or-more digit))
					  "):"
					  (group (one-or-more not-newline))
					  line-end)
				      output)
		    (list (flycheck-error-new-at
			   (car (read-from-string (match-string 2 output)))
			   nil
			   'error
			   (match-string 3 output)
			   ;;:id (match-string 1 output)
			   :checker checker
			   :buffer buffer
			   :filename (buffer-file-name buffer)))))
  :modes snd-scheme-mode)

(add-to-list 'flycheck-checkers 'snd-scheme)
