;;; data.scm                         -*- lexical-binding: t; -*-

;; Copyright (C) 2020-2021  David O'Toole

;; Author: David O'Toole <dto@xelf.me> <deeteeoh1138@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Creating a database

(define^ (hash->plist hash)
  (let ((plist ()))
    (map (lambda (entry)
           (let ((key (car entry))
                 (value (cdr entry)))
             (push! value plist)
             (push! key plist)))
         hash)
    (reverse plist)))

(define^ (plist->hash plist)
  (apply hash-table plist))

(define^ (sexp->file sexp file)
  "Write the Scheme expression SEXP as text to the file FILE."
  (let ((p (open-output-file file)))
    (format p "~W" sexp)
    (close-output-port p)))

(define^ (file->sexp file)
  "Return the Scheme expression previously written to FILE."
  (call-with-input-file file
    (lambda (ip)
      (read ip))))

(define mosaic-data-file-extension ".db.dat")

(define^ (mosaic-data-file file)
  "Return the metadata file name corresponding to FILE."
  (append file mosaic-data-file-extension))

;; modified from examp.scm in snd sources, to prevent stdin/stderr output
(define write-flac* 
  (let ((+documentation+ "(write-flac snd) writes 'snd' in a FLAC file"))
    (lambda (snd)
      ;; write snd data in FLAC format
      (let ((file (append (file-name snd) ".db.wav")))
	(save-sound-as file snd :srate 44100 :header-type mus-riff :sample-type mus-lshort)
	(system (format #f "flac --fast --silent ~A" file))
        (assert (file-exists? file))
        (assert (file-exists? (append (file-name snd) ".db.flac")))
        (format #t "Wrote ~s\n" (append (file-name snd) ".db.flac"))
	(delete-file file)))))

;; this definition replaces the existing one! but it is not
;; called... fix this.  <2020-12-21 Mon> I discovered you need to
;; patch Snd to get --silent to work.
(define read-flac
  (let ((+documentation+ "(read-flac filename) tries to read a FLAC file"))
    (lambda (filename)
      (system (format #f "flac --silent -d ~A" filename)))))

(define^ (mosaic-import-sound file tempo slice-size find-offset read-whole)
  "Slice and analyze FILE at TEMPO with SLICE-SIZE, importing into the
current session's database. Records metadata to FILE.db.dat in the
current session. If FIND-OFFSET is #t, attempt to find the offset of
the first beat using FIND-BEAT. As FIND-BEAT can fail, the default is
#f. Pass #t (or set `mosaic-import-find-offset' to `t' on the Emacs
side) to turn on beat finding."
  (mosaic-select-net (percussion-3.nn))
  (show-progress (list 'import file))
  (forget-all-region-properties)
  (mosaic-new-database)
  (mosaic-close-all-temp-files)
  (close-all-sounds)
  (gc)
  (let* ((sound (open-sound (session-file file)))
         (offset (if find-offset (find-beat sound) 0))
         (tempo (if (eq? #f tempo)
                    (if read-whole
                        100.0
                        (find-tempo (session-file file)))
                    tempo))
         (output-properties ())
         (num-entries 0)
         (file-number 0))
    (set! *mosaic-beats-per-minute* tempo)
    (let* ((slice-size (if (procedure? slice-size)
                           (slice-size)
                           slice-size))
           (regions (scan-sound^ make-region* slice-size sound #f #t offset))
           (num-regions (length regions))
           (n 0)
           (uuids ()))
      (map (lambda (region)
             (remember-region-properties
              '(rms centroid total-energy note notes spectrum pitch pitches
                    kick snare cymbal percussion-3-guess-multiple)
              region))
           regions)
      (let ((grammars `((percussion-3 ,(if read-whole () (structure->grammar (find-percussion-3-structure regions #f))))
                        (note ()))))
        ;;(,structure->grammar (find-note-structure regions))))))
        (display (list 'offset offset))
        (set! *mosaic-slice-size* slice-size)
        (map (lambda (region)
               (let* ((uuid (make-uuid))
                      (data-file (mosaic-data-file (session-file uuid)))
                      (slice-file (session-file uuid)))
                 (push! uuid uuids)
                 (set-region-property! 'uuid region uuid)
                 (set-region-property! 'tempo region tempo)
                 (set-region-property! 'source-file region file)
                 (let ((slice-sound (new-sound :channels 2
                                               :srate (srate sound)
                                               :file slice-file
                                               :header-type mus-riff
                                               :sample-type mus-lshort)))
                   (insert-region* region 0 slice-sound)
                   (write-flac* slice-sound)
                   (close-sound slice-sound)
                   (delete-file slice-file)
                   (push! (*region->properties* region)
                          output-properties)
                   (forget-region-properties region)
                   (mosaic-close-all-temp-files)
                   ;; write out if needed
                   (if (>= num-entries 1024)
                       (let ((output-file (mosaic-data-file (session-file (format #f "~A-~d" file file-number)))))
                         (sexp->file (list () ;; leave grammars blank until the end
                                           output-properties)
                                     output-file)
                         (set! output-properties ())
                         (set! num-entries 0)
                         (set! file-number (+ file-number 1)))
                       ;; just loop
                       (set! num-entries (+ num-entries 1)))
                   (gc)
                   )))
             regions)
        ;; write any remaining entries, plus grammars
        (let ((output-file (mosaic-data-file (session-file (format #f "~A-~d" file file-number)))))
          (sexp->file (list grammars output-properties)
                      output-file))
        ;; clear properties
        (set! *uuid->properties* (make-hash-table 2048 equal?))
        (close-all-sounds)
        (gc)
        (reverse uuids)))))

(define *uuid->properties* (make-hash-table 2048 equal?))

(document^ *uuid->properties*
           "Hash table mapping each slice's UUID to its metadata
properties. This is the current database.")

(define *mosaic-grammars* (make-hash-table 32 equal?))

(define^ (mosaic-new-database)
  "Create a new database. This only clears the Scheme metadata, and
doesn't modify files."
  (set! *uuid->properties* (make-hash-table 32 equal?))
  (set! *mosaic-grammars* (make-hash-table 32 equal?)))

(define^ (mosaic-switch-to-database database)
  "Switch to the database DATABASE for searches."
  (set! *uuid->properties* database))

(define-macro* (mosaic-with-database database :rest body)
  "Evaluate the BODY forms, searching DATABASE."
  `(let ((*uuid->properties* ,database)) ,@body))

(define^ (mosaic-add-entry-to-database entry)
  "Add a piece of slice metadata, the ENTRY, to the current database."
  (let ((uuid (cdr (assoc* 'uuid entry))))
    (set! (*uuid->properties* uuid)
          (delete-properties '(slice-number) entry))))

(define^ (mosaic-load-data-file dat-file (dir (mosaic-session-directory)) extra-properties)
  "Add all the pieces of slice metadata in DAT-FILE to the currently
loaded database table."
  (show-progress (format #f "Loading data file ~s ..." dat-file))
  (let* ((sexp (eval (file->sexp (session-file dat-file dir))))
         (grammars (car sexp))
         (property-entries (cadr sexp))
         (modified-entries (map (lambda (entry)
                                  (append entry extra-properties))
                                property-entries)))
    (when grammars
      (set! (*mosaic-grammars* dat-file) grammars))
    (map mosaic-add-entry-to-database modified-entries))
  dat-file)

(define *database-directory* #f)

(document^ *database-directory*
           "Directory to load .DB.DAT and .DB.FLAC files from.")

(define^ (mosaic-extra-properties-file)
  (session-file "properties.scm"))

(define^ (mosaic-extra-properties)
  (let ((file (mosaic-extra-properties-file)))
    (if (file-exists? file)
        (begin
          (format #t "Loading extra properties from file ~s...\n" file)
          (file->sexp file))
        (begin
          (format #t "No extra properties found.\n")
          ()))))

(define *mosaic-extra-properties* ())

(define^ (mosaic-tag? region tag)
  (let ((entry (assoc* 'tags *region->properties*)))
    (when entry
      (member tag (cdr entry))))) 

(define^ (mosaic-load-database dir)
  "Load all the slice metadata files in DIR into the current
database."
  (let ((all-files (directory->list dir))
        (found-files ())
        (extra-properties (mosaic-extra-properties)))
    (do ((files all-files (cdr files)))
        ((null? files) found-files)
      (let* ((file (car files))
             (pos (string-position ".db.dat" file)))
        (when (number? pos)
                   ;;(= pos (- (length file) 3)))
          (push! file found-files)
          (mosaic-load-data-file file dir extra-properties))))
    ;; store database dir so that we can find FLAC files later
    (set! *database-directory* dir)))
;;    found-files))

(define^ (mosaic-search-database^ func)
  "Call FUNC on each set of properties in the current
database. Returns a list of all slice UUID's for which FUNC returned
#t when given that slice's properties."
  (show-progress "search")
  (let ((results ()))
    (map (lambda (entry)
           (let ((uuid (car entry))
                 (properties (cdr entry)))
             (when (func properties)
               (push! uuid results))))
         *uuid->properties*)
    (display (list 'search-results (length results)))
    (display "\n")
    results))

(define^ (mosaic-snarf-database)
  (show-progress "Snarfing: this may take awhile...")
  (map (lambda (entry)
         (let ((uuid (car entry))
               (properties (cdr entry)))
           (set! (*region->properties* (lazy-region uuid))
                 properties)))
       *uuid->properties*))

(define^ (match-property property value)
  "Return a function matching any one PROPERTY to the value VALUE."
  (lambda (properties)
    (let ((result (assoc* property properties)))
      (when (pair? result)
        (equal? (cdr result) value)))))

(define^ (match-property-with-tolerance property value tolerance)
  "Return a function matching any one PROPERTY to VALUE, within
TOLERANCE."
  (lambda (properties)
    (let ((result (assoc* property properties)))
      (when (pair? result)
        (let ((distance (abs (- value (cdr result)))))
          (< distance tolerance))))))

(define^ (match-tempo-with-tolerance (tolerance 5.0))
  (lambda (properties)
    (match-property-with-tolerance 'tempo (cdr (assoc* 'tempo properties)) tolerance)))

(define^ (match-note note)
  (lambda (properties)
    (let ((entry (assoc* 'notes properties)))
      (when (pair? entry)
        (member note (cdr notes))))))

(define^ (match-and :rest clauses)
  "Return a function that returns #t when all the CLAUSES return #t."
  (eval `(lambda (properties)
           (and ,@(map (lambda (clause)
                        `(,clause properties))
                      clauses)))))

(define^ (match-or :rest clauses)
  "Return a function that returns #t when any the CLAUSES return #t."
  (eval `(lambda (properties)
           (or ,@(map (lambda (clause)
                        `(,clause properties))
                      clauses)))))

(define^ (match-not clause)
  "Return a function that returns the negation of the CLAUSE."
  (eval `(lambda (properties) (not (,clause properties)))))

(define^ (uuid->region uuid)
  "Load and return the audio slice data for UUID from UUID.flac"
  (let ((f (session-file (append uuid ".db.flac") *database-directory*)))
    (if (not (file-exists? f))
        (begin (display (list "Mosaic warning:" f " does not exist."))
               (silent-region (list (make-float-vector 1000) (make-float-vector 1000))))
        (let ((region (sound->region! (open-sound f))))
          (set! (*region->properties* region) (*uuid->properties* uuid))
          region))))

(define^ (lazy-region region-or-uuid)
  (assert (or (pair? region-or-uuid)
              (string? region-or-uuid)))
  (if (string? region-or-uuid)
      (let ((val (uuid->region region-or-uuid)))
        (assert (not (null? val)))
        val)
      region-or-uuid))

(define *search-properties* ())

(document^ *search-properties* "List of property names to try matching during PROPERTIES->QUERY.")

(define^ (match-all properties)
  "A function for the SEARCH-MATCH-SYNTH's PROPERTIES->QUERY field
that matches the entire database. Use this to exhaustively match the
entire database; set it to something else to narrow the search and
speed things up."
  (lambda (p) #t))

(define^ (properties->query properties (operator match-or))
  ;; filter properties to prevent undesirable matches
  (set! properties (delete-properties '(source-file slice-number) properties))
  ;; match'em!
  (eval `(,operator ,@(map (lambda (entry)
                             (if (or (null? *search-properties*)
                                     (and (not (null? *search-properties*))
                                          (member (car entry) *search-properties*)))
                                 (match-property (car entry) (cdr entry))
                                 (lambda (ps) #f)))
                           properties))))

;; Property score functions must keep in mind that A is a loaded
;; region whereas B is a UUID which may not be loaded.

(define^ (properties-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b))
        (num-matches 0))
    (do ((entries ap (cdr entries)))
        ((null? entries) (- 1000 num-matches))
      (let ((entry (assoc* (car (car entries)) bp)))
        (when (and (not (null? entry))
                   (equal? (cdr entry)
                           (cdr (car entries))))
          (set! num-matches (+ 1 num-matches)))))))

(define^ (pairwise-difference p q)
  (assert (and (not (null? p)) (not (null? q))))
  (assert (= (length p) (length q)))
  (if (null? (cdr p))
      (list (- (car p) (car q)))
      (cons (- (car p) (car q))
            (pairwise-difference (cdr p) (cdr q)))))

(define^ (pairwise-square-of-differences p q)
  (assert (and (not (null? p)) (not (null? q))))
  (assert (= (length p) (length q)))
  (map (lambda (x)
         (* x x))
       (pairwise-difference p q)))

(define^ (euclidean-distance p q)
  (assert (and (not (null? p)) (not (null? q))))
  (assert (= (length p) (length q)))
  (sqrt (apply + (pairwise-square-of-differences p q))))

(define^ (spectrum-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b)))
    (assert (not (eq? #f ap)))
    (assert (not (eq? #f bp)))
    (let* ((spectrum-1 (cdr (assoc* 'spectrum ap)))
           (spectrum-2 (cdr (assoc* 'spectrum bp)))
           (freqs-1 (spectrum-freqs spectrum-1))
           (freqs-2 (spectrum-freqs spectrum-2))
           (amps-1 (spectrum-amps spectrum-1))
           (amps-2 (spectrum-amps spectrum-2)))
      (let* ((len (min (length freqs-1) (length freqs-2)))
             (len* (min (length amps-1) (length amps-2))))
        (+ (euclidean-distance (subsequence (vector->list freqs-1) 0 (- len 1))
                               (subsequence (vector->list freqs-2) 0 (- len 1)))
           (euclidean-distance (subsequence (vector->list amps-1) 0 (- len* 1))
                               (subsequence (vector->list amps-2) 0 (- len* 1))))))))

(define^ (pitches-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b)))
    (assert (not (eq? #f ap)))
    (assert (not (eq? #f bp)))
    (let ((freqs-1 (cdr (assoc* 'pitches ap)))
          (freqs-2 (cdr (assoc* 'pitches bp))))
      (let* ((len (min (length freqs-1) (length freqs-2))))
      (euclidean-distance (subsequence (sort! freqs-1 <=) 0 (- len 1))
                          (subsequence (sort! freqs-2 <=) 0 (- len 1)))))))

(define^ (centroid-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b)))
    (assert (not (eq? #f ap)))
    (assert (not (eq? #f bp)))
    (let ((centroid-1 (cdr (assoc* 'centroid ap)))
          (centroid-2 (cdr (assoc* 'centroid bp))))
      (let* ((len (min (length centroid-1) (length centroid-2))))
      (euclidean-distance (subsequence (vector->list centroid-1) 0 (- len 1))
                          (subsequence (vector->list centroid-2) 0 (- len 1)))))))

(define^ (tempo-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b)))
    (if (and (assoc* 'tempo ap)
             (assoc* 'tempo bp))
        (abs (- (cdr (assoc* 'tempo ap))
                (cdr (assoc* 'tempo bp))))
        0.0)))

(define^ (total-energy-distance a b)
  (let ((ap (*region->properties* a))
        (bp (*uuid->properties* b)))
    (if (assoc* 'total-energy ap)
        (abs (- (cdr (assoc* 'total-energy ap))
                (cdr (assoc* 'total-energy bp))))
        0.0)))

(define^ (spectrum-plus-tempo-distance a b)
  (+ (spectrum-distance a b)
     ;; assign less weight to tempo distance
     (* 0.2 (tempo-distance a b))))

(define^ (weighted-distance (spectrum 1.0) (centroid 1.0) (tempo 0.1) (energy 0.1) (pitches 0.0))
  "Returns a SCORE-FUNCTION where SPECTRUM is the coefficient of the
spectrum distance, CENTROID is the coefficient of the centroid
distance, and TEMPO is the coefficient of the tempo distance. The
centroid is a measure of a spectrum's `center of gravity'. Limiting
tempo distance can help avoid excessive use of STRETCH-MATCH-SYNTH."
  (lambda (a b)
    (+ (* spectrum (spectrum-distance a b))
       (* centroid (centroid-distance a b))
       (* tempo (tempo-distance a b))
       (* energy (total-energy-distance a b))
       (* pitches (pitches-distance a b)))))
       
