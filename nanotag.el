;;; nanotag.el --- tiny source code navigation utility  -*- lexical-binding: t; -*-

;; Copyright (C) 2019-2021  David O'Toole

;; Author: David O'Toole <dto@monad>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; You can use the following Emacs Lisp code to navigate lisp files by
;; pressing a chosen key to cycle through all the file's occurrences
;; of a given angle-bracketed tag. This can be used as a simple cross
;; referencing system. When you press F5 (or your chosen key) on a
;; line with a tag, the function <FIND-TAG> will move the Emacs cursor
;; to the next occurrence of the tag. The search will wrap around to
;; the top of the file when you reach the end. If you are not on a
;; line with a tag, it will search backward for a tag and then search
;; forward for its next occurrence.

;; You can use F4 (or another chosen key) to navigate to a tag whose
;; name you enter, with TAB completion of all available tags in the
;; file.

;; Functions/keybindings for navigating program text.

;;; Code:

(defun <find-tag> (&optional key) 
   (interactive)
   (let ((keyword (or key (save-excursion
			   (end-of-line)
			   (re-search-backward "<[^>].*>")
			   (match-string 0)))))
     (when (not (search-forward keyword nil 'noerror))
       (goto-char (point-min))
       (search-forward keyword))))

(defun <find-all-tags> ()
  (interactive)
  (let (tags)
    (save-excursion
     (save-match-data
      (goto-char (point-min))
      (while (re-search-forward "<[^>].*>" nil :no-error)
	     (let ((match (match-string 0)))
	       (when match
		 (setq tags (cons match tags)))))))
    tags))

(defun <find-tag-interactively> ()
  (interactive)
  (<find-tag> (completing-read "Find <tag> interactively: "
			       (<find-all-tags>)
			       nil nil "<" nil
			       "<")))

(global-set-key (kbd "<f5>") '<find-tag>)
(global-set-key (kbd "<f4>") '<find-tag-interactively>)

(provide 'nanotag)
;;; nanotag.el ends here
