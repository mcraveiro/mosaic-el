;;; hydrogen.el --- export beats to Hydrogen/LMMS    -*- lexical-binding: t; -*-

;; Copyright (C) 2021  David O'Toole

;; Author: David O'Toole <deeteeoh1138@gmail.com>
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; 

;;; Code:

(defvar mosaic-drumkit-archive-extension ".h2drumkit")
(defvar mosaic-drumkit-data-file-name "drumkit.xml")

(defvar mosaic-drumkit-header-format
  "<drumkit_info>
<name>
%s
</name>
<author>
%s
</author>
<info>
%s
</info>
<license>
%s
</license>
<instrumentList>
")

(defvar mosaic-drumkit-instrument-format
"   <instrument>
        <id>0</id>
        <name>Kick drum</name>
        <volume>1</volume>
        <isMuted>false</isMuted>
        <pan_L>1</pan_L>
        <pan_R>1</pan_R>
        <randomPitchFactor>0</randomPitchFactor>
        <filterActive>false</filterActive>
        <filterCutoff>1</filterCutoff>
        <filterResonance>0</filterResonance>
        <Attack>0</Attack>
        <Decay>0</Decay>
        <Sustain>1</Sustain>
        <Release>1000</Release>
        <exclude />
")

(defvar mosaic-drumkit-layer-format
"
     <layer>
      <filename>35kick.flac</filename>
      <min>0</min>
      <max>1</max>
      <gain>1</gain>
      <pitch>0</pitch>
    </layer>
    </instrument>
")

(defvar mosaic-drumkit-footer-format
"</instrumentList>
</drumkit_info>
")

(provide 'hydrogen)
;;; hydrogen.el ends here

